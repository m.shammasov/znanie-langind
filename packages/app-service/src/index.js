const path =  require('path')
const Fastify = require('fastify')
const fastifyStatic = require('fastify-static')
const fs = require("fs")
const root = path.join(__dirname, '..', '..', 'app-front', 'public')
const fetch = require('isomorphic-fetch')
const run  = async () => {
    let fastify = Fastify({
        ignoreTrailingSlash: true,
        bodyLimit: 1048576 * 4,
        logger: {
            level: 'debug',
        },
        trustProxy: true,
        // ?modifyCoreObjects:false"
    });

    fastify.register(require('fastify-cors'), {origin: false,})

    fastify.get('/share/:id', async (request, reply) => {

        const id = request.params.id

        if(id) {
            console.log('card id', id)
            const result = await fetch('https://api.spasibozaznanie.ru/card/'+id).then( r => r.json())
            const gid = result.result
            if(gid) {
                console.log('gid', gid)
                let htmlData = fs.readFileSync(path.join('..','app-front','public','index.html'), 'utf8')
                htmlData = htmlData
                    //replace('Узнай, какую номинацию получишь ты!', 'Хочу забрать свою награду и приЗнание!')
                  //  .replace('Пройди тест и поделись своими результатами:', 'Узнай, какую номинацию получишь ты!')
                    .replace('https://spasibozaznanie.ru/FB.png', gid)
                    .replace(`<meta property="og:image:width" content="1200">`, `<meta property="og:image:width" content="1200">`)
                    .replace(`<meta property="og:image:height" content="630">`, `<meta property="og:image:height" content="630">`)
                    return reply.type('text/html').send(htmlData);
            }
        }

        return reply.sendFile('index.html')
    })
    
    const indexRoutes = ['/', '/index', '/index/*', '/index.html', '/start*', "/end*"]

    indexRoutes.forEach( path => 
        fastify.get(path, (req, reply) => {
            return reply.sendFile('index.html')
        })
    )

    fastify.register(fastifyStatic, {root})
    await fastify.listen(3031)

}

run()