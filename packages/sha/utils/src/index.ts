export * from './environment'
export * from './async'
export * from './array'

export const capitalize = (value = '') =>
    value.substr(0, 1).toUpperCase() + value.substring(1)

