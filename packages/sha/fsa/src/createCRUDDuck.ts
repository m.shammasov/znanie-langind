import * as FSA from './fsa'
import * as R from 'ramda'
import {DeepPartial} from 'utility-types'
import {FactoryAnyAction, isNamespace} from './fsa'
import {Reducer} from 'redux'

const createCRUDDuck = <T = any, ID extends keyof T = any>(
    factoryPrefix: string,
    idProp: ID,
    defaultProps: DeepPartial<T> = {} as any,
    defaultPersistent = true,
    get = (state: any): T[] => state.app[factoryPrefix]
) => {

    const factory = FSA.actionCreatorFactory(factoryPrefix, {persistent: defaultPersistent})
    type PatchBase = {[P in ID]: string}
    const patchCreator = factory<T>('patched', {persistent: defaultPersistent})
    const addedRaw = factory<T>('added',  {persistent: defaultPersistent})

    const actions = {
        reseted: factory<T[]>('reseted',  {persistent: false}),

        added: addedRaw,
        requested: factory<ID>('requested',  {persistent: false}),
        removed: factory<ID>('removed',  {persistent: defaultPersistent}),
        patched: Object.assign((patch: PatchBase & Partial<T>, original: Partial<T> = {}) => {
            const diff = {
                [idProp]: patch[idProp]
            }

            let diffFlag = false
            for (const [key, value] of Object.entries(patch)) {
                if(!R.equals(patch[key], original[key])) {
                    diff[key] = patch[key]
                    diffFlag = true
                }
            }

            return diffFlag
            // @ts-ignore
                ? patchCreator({...diff, [idProp]: patch[idProp] || original[idProp]})
                : undefined
        },
        {
            ...patchCreator
        }
        ),
        updated: factory<T>('updated', {persistent: defaultPersistent}),
    }

    const commands = {
        requestAdd: factory<T>('requestAdd'),
        requestRemove: factory<ID>('requestRemove'),
        requestPatch: factory<Partial<T>>('requestPatch'),
        requestUpdate: factory<T>('requestUpdate')
    }
    const reducer = FSA.reducerWithInitialState([])
        .case(
            actions.reseted,
            (_, payload) => payload,
        )
        .case(
            actions.added,
            // @ts-ignore
            (state, payload) => R.append(R.mergeDeepRight(defaultProps, payload), state),
        )
        .case(
            actions.removed,
            (state, payload) => R.reject(obj => obj[idProp] === payload, state),
        )
        .case(
            actions.updated,
            (state, payload) => {
                const index = R.findIndex(
                    R.propEq(
                        idProp as string | number,
                        payload[idProp],
                    ),
                    state)
                if(index === -1)
                    return R.prepend(R.mergeDeepRight(defaultProps, payload), state)
                const lens = R.lensIndex(
                    index
                )
                return R.set(lens, payload, state)
            }

        )
        .case(
            actions.patched,
            (state, payload) => {
                const index =    R.findIndex(
                    R.propEq(
                        idProp as string | number,
                        payload[idProp],
                    ),
                    state)
                if(index === -1)
                    return [...state, payload]


                const arr = [...state]
                let element
                const source = state[index]
                try {
                    element  = R.mergeDeepRight(source, payload)
                }
                catch (e) {
                    debugger
                }
                arr[index] = element
                return arr
            }
        )

    const selectById =  (id: string) => (state: any) => {
        const array: T[] = get ? get(state) : state

        for (let i = 0; i < array.length; i ++)
            // @ts-ignore
            if (array[i][idProp] === id )
                return array[i]

        // throw new Error(factoryPrefix + ' with id '+id + ' not found')
        return undefined
    }

    const selectPropEq = <K extends (keyof T)>(key: K)=> (value: T[K]) => (state: any) => {
        const array: T[] = get ? get(state) : state
        const items: T[] = []
        for (let i = 0; i < array.length; i ++)
            // @ts-ignore
            if (array[i][key] === value )
                items.push(array[i])

        return items
    }


    const concatItemReducer = (handleItem:  (state:T, action: any) => T) => {
        const result = (state: T[] = [], action) => {
            state = reducer(state, action)


            if (isNamespace(factory)(action)) {
                const id = action.payload[idProp]
                if (id) {
                    const prevItem = state.find(item => item[idProp] === id)
                    const itemIndex = state.findIndex(item => item[idProp] === id)
                    if (prevItem) {
                        const newItem = handleItem(prevItem, action)
                        if (newItem !== prevItem) {


                            state = [...state]
                            state[itemIndex] = newItem
                        }
                    }
                }
            }

            return state
        }

        return Object.assign(result, {duck}) as any as Reducer<T[], FactoryAnyAction> & {duck: any}
    }
    const duck = {
        factoryPrefix,
        idProp,
        idKey: idProp ,
        factory,
        actions: {...actions, ...commands},
        reducer: reducer as any as (state: T[], action: FactoryAnyAction) => T[],
        concatItemReducer,
        isValid: (state, action) => {

            if(
                actions.added.isType(action)
            ) {

                const idToAdd = action.payload[idProp]
                // @ts-ignore
                if (selectById(idToAdd)(state)) {
                    return 'Id ' + idToAdd + ' already exists in ' + factoryPrefix + ' collection'
                }
            }
            return undefined
        },
        optics: {
            selectAll: state => {
                const array: T[] = get ? get(state) : state
                return array
            },
            selectAllExceptById:   (id: string) => (state: any) => {
                const array: T[] = get ? get(state) : state
                const result:T[] = []
                for (let i = 0; i < array.length; i ++)
                    // @ts-ignore
                    if (array[i][idProp] !== id )
                        result.push(array[i])

                // throw new Error(factoryPrefix + ' with id '+id + ' not found')
                return result
            },
            selectById:  (id: string) => (state: any) => {
                const array: T[] = get ? get(state) : state

                for (let i = 0; i < array.length; i ++)
                    // @ts-ignore
                    if (array[i][idProp] === id )
                        return array[i]

                // throw new Error(factoryPrefix + ' with id '+id + ' not found')
                return undefined
            },
            selectPropEq,
            selectEq: (query: Partial<T>) => (state: any) => {
                const array: T[] = get ? get(state) : state
                const items: T[] = R.filter(R.whereEq<Partial<T>>(query), array)

                return items
            },
            selectEqOne: (query: Partial<T>) => (state: any): T => {
                const array: T[] = get ? get(state) : state
                const items: T[] = R.filter(R.whereEq<Partial<T>>(query), array)

                return items[0]
            },
            select: (query: Record<keyof Partial<T>, any>) => (state: any) => {
                const array: T[] = get ? get(state) : state
                const items: T[] = []
                for (let i = 0; i < array.length; i++){
                    if(R.where<Partial<T>>(query)(array[i]))
                        items.push(array[i])
                }

                return items
            },

        }
    }

    return duck
}

export type Duck<T, ID> = ReturnType<typeof createCRUDDuck>
export default createCRUDDuck
