import type { StorybookConfig } from '@storybook/react/types';

const config: StorybookConfig = {
    stories: ['./src/**/*.story.*'],
    logLevel: 'debug',
    addons: [
        '@storybook/addon-essentials',
        '@storybook/addon-controls',
        '@storybook/addon-storysource',
        {
            name: '@storybook/addon-docs',
            options: {
                sourceLoaderOptions: {
                    parser: 'typescript',
                    injectStoryParameters: false,
                },
            },
        },
    ],
    core: {
        builder: 'webpack5',
    },
    typescript: {
        check: false,
        checkOptions: {},
        reactDocgenTypescriptOptions: {
            propFilter: (prop) => ['label', 'disabled'].includes(prop.name),
        },
    },
};

module.exports = config;
