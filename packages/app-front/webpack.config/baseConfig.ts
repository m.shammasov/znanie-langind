import path from 'path'
import webpack from 'webpack'
import * as WebpackDevServer from 'webpack-dev-server'

const cfg: WebpackDevServer.Configuration & webpack.Configuration =  {
    entry: {
        index: './src/index.tsx',
    },

    output: {
        filename: '[name].[contenthash].js',
        path: path.resolve(__dirname, '../public'),
        publicPath: '/',
    },

    ignoreWarnings: [/Failed to parse source map/],

    plugins: [
        new webpack.ContextReplacementPlugin(/moment[/\\]locale$/, /ru/),
        // new webpack.IgnorePlugin( /^mongoose/),
        /** new webpack.DefinePlugin({
          __VERSION__: JSON.stringify(__VERSION__)
        })*/
    ],

    resolve: {
    // Add '.ts' and '.tsx' as resolvable extensions.
        extensions: ['.ts', '.tsx', '.js', '.jsx', '.json'],
        fallback: {
            assert: require.resolve('assert/'),
            buffer: require.resolve('buffer/'),
            console: require.resolve('console-browserify'),
            constants: require.resolve('constants-browserify'),
            crypto: require.resolve('crypto-browserify'),
            domain: require.resolve('domain-browser'),
            events: require.resolve('events/'),
            http: require.resolve('stream-http/'),
            https: require.resolve('https-browserify'),
            os: require.resolve('os-browserify/browser'),
            path: require.resolve('path-browserify'),
            punycode: require.resolve('punycode/'),
            process: require.resolve('process/browser'),
            querystring: require.resolve('querystring-es3'),
            stream: require.resolve('stream-browserify'),
            string_decoder: require.resolve('string_decoder/'),
            sys: require.resolve('util/'),
            timers: require.resolve('timers-browserify'),
            tty: require.resolve('tty-browserify'),
            url: require.resolve('url/'),
            util: require.resolve('util/'),
            vm: require.resolve('vm-browserify'),
            zlib: require.resolve('browserify-zlib'),
        },
    },

    module: {
        rules: [
            {
                test: /\.css$/,
                use: [
                    {
                        loader: 'style-loader',
                    },
                    {
                        loader: 'css-loader',
                    },
                    {
                        loader: 'postcss-loader'
                    },
                ],
            },
            {
                test: /\.less$/,
                use: [{
                    loader: 'style-loader',
                }, {
                    loader: 'css-loader', // translates CSS into CommonJS
                }, {
                    loader: 'less-loader', // compiles Less to CSS
                    options: {
                        lessOptions: { // If you are using less-loader@5 please spread the lessOptions to options directly
                            modifyVars: {
                                'primary-color': '#ff0000',
                                'link-color': '#1DA57A',
                                'border-radius-base': '12px',
                            },
                            javascriptEnabled: true,
                        },
                    },
                }],
            },
            {
                test: /\.s[ac]ss$/i,
                use: [
                    // Creates `style` nodes from JS strings
                    "style-loader",
                    // Translates CSS into CommonJS
                    "css-loader",
                    // Compiles Sass to CSS
                    "sass-loader",
                ],
            },
            {
                test: /.ts?$|.tsx?$/,
                exclude: /\.story\.tsx?$/,
                use: [
                    {
                        loader: 'ts-loader',
                        options: {
                            // disable type checker - we will use it in fork plugin
                            transpileOnly: true,
                            allowTsInNodeModules: true
                        },
                    },
                ],
            },
            // All output '.js' files will have any sourcemaps re-processed by 'source-map-loader'.
            {
                enforce: 'pre',
                test: /\.js$/,
                loader: 'source-map-loader',
            },
        ],
    },


}


export default cfg
