import merge from 'webpack-merge'
import * as path from 'path'
import Dotenv from 'dotenv-webpack'
const HtmlWebpackPlugin = require('html-webpack-plugin')

import * as webpack from 'webpack'
import * as WebpackDevServer from 'webpack-dev-server'
import {DefinePlugin} from 'webpack'
import moment from 'moment'
import baseConfig from './baseConfig'
import * as R from 'ramda'

const defaultOptions = {
    LTV_FRONT_host: '0.0.0.0',
    LTV_FRONT_port: 3031,
    LTV_FRONT_open: 'http://localhost:3031/'
}

export type WebpackBuildOptions = typeof defaultOptions

const pickLTVVars = R.pick(Object.keys(defaultOptions))

const processLTVEnv: any = pickLTVVars(process.env)

const withDefaultOptions = {...defaultOptions, ...processLTVEnv}

export default (inputOptions: Partial<WebpackBuildOptions> = {}) => {
    console.log('Input env options', inputOptions)
    const options = {...withDefaultOptions, ...inputOptions}
    console.log('Merged env options', options)
    const devServer: WebpackDevServer.Configuration = {
        static: {
            directory: path.resolve(__dirname, '../public/'),
        },
        allowedHosts: 'all',
        historyApiFallback: {

            disableDotRule: true,
            logger: console.log.bind(console),
            verbose: true,
            /* rewrites: [
              { from: , to: '/index.html' }
            ]*/
        },
        proxy:{
            '/upload': 'http://api.spasibozaznanie.ru/',
            '/card': 'http://api.spasibozaznanie.ru/',
            '/profane/': 'https://api.spasibozaznanie.ru/',
        },
        port: options.LTV_FRONT_port || 80,
        host: options.LTV_FRONT_host || '',
        hot: true,
        open: options.LTV_FRONT_open,
        headers: {'Access-Control-Allow-Origin': '*'},
    }
    const configuration = {

        // @ts-ignore
        mode:'development',

        plugins: baseConfig.plugins.concat([
            new HtmlWebpackPlugin({
                template: path.resolve(__dirname, '../template/index.html'),
                filename: path.resolve(__dirname, '../public/index.html'),
            }),
            new DefinePlugin({
                'process.env': JSON.stringify({
                    LTV_FRONT_LOG_ROCKET: options.LTV_FRONT_LOG_ROCKET,
                    LTV_FRONT_API_BASE: options.LTV_FRONT_API_BASE,
                    LTV_FRONT_BI_MOCKS: options.LTV_FRONT_BI_MOCKS,
                    WEBPACK_SERVE: options['WEBPACK_SERVE'],
                    BUILD_TIME: moment().format('YYYY/MM/DD HH:mm')
                })
            })
        ]),
        devServer: options['WEBPACK_SERVE'] ? devServer : undefined
    }

    return merge<any>(baseConfig, configuration)

}
