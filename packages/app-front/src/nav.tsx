const nav = {
    startFirst: '/start/first',
    startSecond: '/start/second',
    startChat: '/start/chat',

    endResult: '/end/result',
    endNomination: '/end/nomination',
    endSupport: '/end/support',

    share: '/share/'
}

export default nav
