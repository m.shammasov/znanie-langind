import {combineReducers} from "redux";
import {uiDuck} from "./ducks/uiDuck";
import {messagesDuck} from "./ducks/messagesDuck";
import {connectRouter, getBrowserHistory, RouterState} from '@sha/router'
import {cardDuck} from "./ducks/cardDuck";

const rootReducer = (history) => combineReducers({
    ui: uiDuck.reducer,
    messages: messagesDuck.reducer,
    card: cardDuck.reducer,
    router: connectRouter(history)
})
export default rootReducer



export type RootState = ReturnType<ReturnType<typeof rootReducer>>
