import * as FSA from '@sha/fsa'
export type Message = {
    guid: string
    side: 'user' | 'bot',
    text?: string
    type?: 'text' | 'photo'| 'character'
}


const factory = FSA.actionCreatorFactory('messages')

const actions = {
    botSent: factory<string>('botSent'),
    userSent: factory<{ text: string, type:  'text' | 'photo' | 'character'}>('userSent'),
    reset: factory<undefined>('reset')
}

const reducer = (state: Message[] = [], action) => {
    if(actions.botSent.isType(action))
        return  [...state, {side: 'bot', text: action.payload, type: 'text'}]
    if(actions.userSent.isType(action))
        return  [...state, {side: 'user', ...action.payload}]
    if(actions.reset.isType(action))
        return  []

    return state
}

export const messagesDuck = {
    reducer,
    actions,
    factory,
    selectMessages: state => state.messages as Message[]
}