import * as fsa from '@sha/fsa'
import {FactoryAnyAction} from '@sha/fsa'
import {append, equals, reject} from 'ramda'
import {combineReducers} from 'redux'
import {UserVO} from '../../api/apiUser'
import {GeneralFiltersMap} from '../../api/apiDataProxy'
import {preloadersDuck} from './preloadersDuck'
import {FrontState} from '../frontReducer'
import * as R from 'ramda'
import {ArgsProps, NoticeType} from 'antd/lib/message'
import * as React from 'react'
import {createBootstrapComponent} from "react-bootstrap/ThemeProvider";
export type UIState = ReturnType<typeof reducer>

const factory = fsa.actionCreatorFactory('ui')
const actions = {
    busy: factory<any>('busy'),
    unbusy: factory<any>('unbusy'),
    setStep: factory<number>('setStep'),
    setTurn: factory<'user'|'bot'>('setTurn'),
    setShowShareMainModal: factory<boolean>('setShowShareMainModal'),
    setShowShareCopyModal: factory<'result' | 'main' |false>('setShowShareCopeModal'),

    setShowShareOkModal: factory<boolean>('setShowShareOkModal')
}


const busyReducers = (state: any[] = ['preload'], action: FactoryAnyAction): any[] => {
    if (actions.busy.isType(action))
        return append(action.payload, state)
    if (actions.unbusy.isType(action))
        // @ts-ignore
        return  reject(equals(action.payload), state)
    if (actions.unbusy.isType(action))
        // @ts-ignore
        return  reject(equals(action.payload), state)
    return state
}

const stepReducers = (state: number = 0, action: FactoryAnyAction): number => {
    if (actions.setStep.isType(action))
        return action.payload

    return state
}



const reducer = combineReducers({
    step:stepReducers,
    busy: busyReducers,
    showShareCopyModal:  (state =  false, action) => {
    if (actions.setShowShareCopyModal.isType(action))
        return action.payload

    return state
},

    showShareOkModal:  (state = false, action) => {
        if (actions.setShowShareOkModal.isType(action))
            return action.payload

        return state
    },
    turn: (state: 'user' | 'bot' = 'bot' , action) => {
        if (actions.setTurn.isType(action))
            return action.payload

        return state
    }
})


export const selectUI = (state): UIState =>
    state.ui


export const uiDuck = {
    actions,
    reducer,
    selectUI
}
