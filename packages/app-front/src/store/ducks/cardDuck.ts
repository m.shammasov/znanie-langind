import * as fsa from '@sha/fsa'
import {FactoryAnyAction} from '@sha/fsa'
import {Card} from "../../api/card"
import {RootState} from "../rootReducer";
const factory = fsa.actionCreatorFactory('card')

const actions = {
    setName: factory<string>('setName'),
    setActivity: factory<number>('setActivity'),
    setPhoto: factory<{fileName:string, savedName: string}>('setPhoto'),
    setCharacter: factory<number>('setCharacter'),
    setComplete: factory<undefined>('setComplete'),
    setId: factory<string>('setId'),
    setResult: factory<string>('setResult'),
    reset: factory<undefined>('reset'),
    replace: factory<Card>('replace'),

}

const reducer = (state: Partial<Card> = {}, action: FactoryAnyAction): Partial<Card> => {
    if (actions.replace.isType(action))
        return {...action.payload}
    if (actions.setId.isType(action))
        return {...state, id: action.payload}
    if (actions.setName.isType(action))
        return {...state, name: action.payload}
    if (actions.setResult.isType(action))
        return {...state, result: action.payload}
    if (actions.setActivity.isType(action))
        return {...state, activity: action.payload}
    if (actions.setCharacter.isType(action))
        return {...state, character: action.payload}
    if (actions.setPhoto.isType(action))
        return {...state, photo: action.payload.savedName}
    if (actions.reset.isType(action))
        return {}
    if (actions.replace.isType(action))
        return {...action.payload}
    return state
}

const selectShareLink = (state) =>
    window.location.origin+'/share/' + state.card.id

const vkShare = ({url= window.location.origin, title='Социальная премия «ПриЗнание»', image = 'https://spasibozaznanie.ru/VK.png'}) => {
let img = image.replace('_1200.png', '_537.png')
return "https://vk.com/share.php?" +
        "url="+encodeURIComponent(url)+
        "&title="+encodeURIComponent(title)+
        '&image='+encodeURIComponent(img)
}
const okShare = ({url=  window.location.origin, title='Социальная премия «ПриЗнание»', image = 'https://spasibozaznanie.ru/OK.png'}) => {
    let img = image.replace('_1200.png', '_640.png')
return "https://connect.ok.ru/offer?" +
        "url="+encodeURIComponent(url)+
        "&title="+encodeURIComponent(title)+
        '&imageUrl='+encodeURIComponent(img)

}

const fbShare = ({url=  window.location.origin, title='Социальная премия «ПриЗнание»', image = 'https://spasibozaznanie.ru/FB.png'}) => {
    return "http://www.facebook.com/sharer/sharer.php?" +
        "u="+encodeURIComponent(url)+
        "&title="+encodeURIComponent(title)+
        '&picture='+encodeURIComponent(image)+
        '&p[images][0]='+encodeURIComponent(image)
}
export const cardDuck = {
    actions,
    reducer,
    selectCard: (state) => state.card,
    selectShareLink,
    selectShareLinks: (state: RootState) => {
        const url = selectShareLink(state)
        const encoded = encodeURIComponent(url)
        const title =  'Хочу забрать свою награду и приЗнание!'
        const options = {url, title, image: state.card.result}
        return {
            ok: okShare(options),
            vk: vkShare(options),
            fb: fbShare(options),
        }
    },
    selectShareMainLink: (state?) => {
        return window.location.origin
    },
    selectShareMainLinks: (state) => {
        const url = selectShareLink(state)
        const encoded = encodeURIComponent(window.location.origin)
        return {
            ok: okShare({}),
            vk: vkShare({}),
            fb: fbShare({}),
        }
    },
}

