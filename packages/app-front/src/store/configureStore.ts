import {ActionCreator, applyMiddleware, compose, createStore, Reducer, Store} from 'redux'
import createSagaMiddleware from 'redux-saga'

import {routerMiddleware} from '@sha/router'
import rootReducer, {RootState} from './rootReducer'

import rootSaga from './rootSaga'
import {getBrowserHistory} from '@sha/router/src'

import {isArray, isBrowser} from '@sha/utils'
import * as R from 'ramda'

export const configureFrontStore = (
    initialState?: RootState,
    historyInstance: ReturnType<typeof getBrowserHistory> = getBrowserHistory(),
    startSaga = rootSaga
):  Store<RootState> & { runSaga: SagaRunFunction} => {

    // @ts-ignore
    const store = createStore(rootReducer(historyInstance), initialState, getFrontEndMiddleware(historyInstance))

    // @ts-ignore
    store['runSaga'] = sagaMiddleware.run
    if(startSaga)
        sagaMiddleware.run(startSaga, historyInstance)

    // @ts-ignore
    return store
}



export let singletonFrontendStore

type SagaRunFunction = typeof sagaMiddleware['run']
const sagaMiddleware = createSagaMiddleware()

import {composeWithDevTools} from 'redux-devtools-extension'

import listenActionEnhancer from './listenActionEnhancer'

const composeEnhancers = composeWithDevTools({
    name: 'znanie',
    trace: true,
    traceLimit: 25,
    latency: 200,
    maxAge: 200,
});

const getFrontEndMiddleware = (history?: any) =>
    isBrowser()
        ?
        composeEnhancers(

            // applyMiddleware(sagaMiddleware),
            applyMiddleware(routerMiddleware(history), sagaMiddleware),// , LogRocket.reduxMiddleware()),
            listenActionEnhancer,
        )
        :
        compose(
            // applyMiddleware(sagaMiddleware),
            applyMiddleware(routerMiddleware(history), sagaMiddleware) // , LogRocket.reduxMiddleware()),
        )
