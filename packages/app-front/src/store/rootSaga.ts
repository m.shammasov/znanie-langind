import { takeLatest, call , take} from "@redux-saga/core/effects";
import * as FSA from '@sha/fsa'
import {messagesDuck} from "./ducks/messagesDuck";
import {put, select} from "redux-saga/effects";
import {cardDuck} from "./ducks/cardDuck";
import {uiDuck} from "./ducks/uiDuck";
import {sleep} from "@sha/utils"
import {activities} from "../api/activities"
import { router, createBrowserHistory } from 'redux-saga-router'
import {isAnyLocationAction, isLocationAction} from "@sha/router/src"
import characters from "../api/characters";

import * as RR from 'connected-react-router'
import nav from "../nav";
import {API_BASE} from "../constants";
import {RootState} from "./rootReducer";
import {useSelector} from "react-redux";
import {Card} from "../api/card";
export const chatFactory = FSA.actionCreatorFactory('chat')
export const chatActions = {
    start: chatFactory<never>('start'),

}
const actions = chatActions
export default function* rootSaga(history) {
    const state: RootState = yield select(state => state )
    if(state.router.location.pathname.startsWith('/share/')) {
        const id = state.router.location.pathname.split('/').pop()
        try {
            const card = yield call(fetchCard, id)
            yield put(cardDuck.actions.replace({id, ...card}))
        } catch(e) {
            yield put(RR.replace(nav.startFirst))
        }
    } else if(state.router.location.pathname.startsWith('/end')) {
        yield put(RR.replace(nav.startFirst))
    }
    yield put(uiDuck.actions.unbusy('preload'))
    yield takeLatest(a => isAnyLocationAction(a) && a.payload.location.pathname === '/start/chat', startChat)
}

const timeout = 1

const checkWord = async (word: string) => {
    const result = await (fetch('https://api.spasibozaznanie.ru/profane/'+word).then(res => res.json()))
    return result.clean === 'True'
}

const fetchCard = async (id: string): Promise<Card> => {
    const result = await (fetch('https://api.spasibozaznanie.ru/card/'+id).then(res => res.json()))
    return result
}

function* startChat() {
    console.log('start chat')
    yield put(uiDuck.actions.setTurn('bot'))
    yield put(messagesDuck.actions.reset(undefined))
    yield put(cardDuck.actions.reset(undefined))

    yield call(sleep, timeout)
    yield put(messagesDuck.actions.botSent(` Привет! Я – Бот Награждаевич, твой персональный проводник премии «Знание». Давай узнаем, какую персональную награду ты можешь получить!😉`))
    yield call(sleep, timeout)
    yield put(messagesDuck.actions.botSent(`Как тебя зовут ?`))

    let name
    do {
        yield put(uiDuck.actions.setTurn('user'))
        yield put(uiDuck.actions.setStep(1))
        let setNameAction = yield take(cardDuck.actions.setName.isType)

        const word = setNameAction.payload.trim()
        yield put(uiDuck.actions.setTurn('bot'))
        yield call(sleep, timeout)

        const wordsLength = word.split(' ').length

        if (word.length < 3 ) {

            yield put(messagesDuck.actions.userSent({text: `Меня зовут: ${word}`, type: 'text'}))
            yield put(messagesDuck.actions.botSent(`Слишком короткое, подскажи, как тебя зовут ?`))
        } else if(wordsLength > 1) {

            yield put(messagesDuck.actions.userSent({text: `Меня зовут: ${word}`, type: 'text'}))
            yield put(messagesDuck.actions.botSent(`Имя может включать только одно слово, подскажи, как тебя зовут ?`))
        }
        else{
            try {
                const isClean = yield call(checkWord, word)
                if (!isClean) {

                    yield put(messagesDuck.actions.userSent({text: `Меня зовут: *****`, type: 'text'}))
                    yield put(messagesDuck.actions.botSent(`Не корректное имя, подскажи, как тебя зовут ?`))
                } else {
                    yield put(messagesDuck.actions.userSent({text: `Меня зовут: ${word}`, type: 'text'}))
                    name = word
                }
            }   catch (e) {
                yield put(messagesDuck.actions.userSent({text: `Меня зовут: `+word, type: 'text'}))
                yield put(messagesDuck.actions.botSent(`Ошибка, неполадки с соединением. Проверь подключение к сети и попробуй ввести своё имя занов !`))
            }
        }
        yield call(sleep, timeout)
    }while (!name)

    yield put(messagesDuck.actions.botSent(`Какие у тебя увлечения?`))
    yield put(uiDuck.actions.setTurn('user'))
    yield put(uiDuck.actions.setStep(2))
    const setActivityAction = yield take(cardDuck.actions.setActivity.isType)
    yield put(messagesDuck.actions.userSent({text: activities[setActivityAction.payload].title, type: 'text'}))
    yield put(cardDuck.actions.setCharacter(setActivityAction.payload))

    yield put(uiDuck.actions.setTurn('bot'))
    yield call(sleep, timeout)
    yield put(messagesDuck.actions.botSent(`Сделай фото, чтобы было видно твое лицо`))
    yield put(uiDuck.actions.setTurn('user'))
    yield put(uiDuck.actions.setStep(3))
    const setPhotoAction = yield take(cardDuck.actions.setPhoto.isType)
    yield put(messagesDuck.actions.userSent({text: setPhotoAction.payload.fileName, type: 'text'}))
    yield put(uiDuck.actions.setTurn('bot'))
        /*
    yield call(sleep, timeout)
    yield put(messagesDuck.actions.botSent(`Твоя премия вот-вот будет готова. А в каком образе ты придешь на награждение?`))
    yield put(uiDuck.actions.setTurn('user'))
    yield put(uiDuck.actions.setStep(4))
    const setCharacterAction = yield take(cardDuck.actions.setCharacter.isType)
    yield put(messagesDuck.actions.userSent({text: characters[setCharacterAction.payload].previewUrl, type: 'character'}))
    yield put(uiDuck.actions.setTurn('bot'))
*/
    yield call(sleep, timeout)
    yield put(messagesDuck.actions.botSent(`Отлично, твоя номинация готова!`))
    yield put(uiDuck.actions.setTurn('user'))
    yield put(uiDuck.actions.setStep(5))
    const setCompleteAction = yield take(cardDuck.actions.setComplete.isType)

    const state: RootState = yield select()
    const postCard = async () => {
        const result = await fetch('https://api.spasibozaznanie.ru/card/', {method: 'POST', headers:{
                'Content-Type': 'application/json',
                Authorization: 'Token eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoiYWNjZXNzIiwiZXhwIjoxNjI3OTk4MzM1LCJqdGkiOiI5NzNiNTgzMGVjYjM0NTM4YjRhYTBmMDY2MzUwNjVhNiIsInVzZXJfaWQiOjZ9.KAf70HBPbnYRQ7okEJyfQ1uC4AG6ofui7ndSVtUoONc'
            },

            body: JSON.stringify(state.card)

        }).then(r => r.json())
        return result.id
    }
    const id = yield call(postCard)
    const getSocialImage = async () => {
        const result = await fetch(' https://api.spasibozaznanie.ru/card/'+id)
            .then(r => r.json())
        return result

    }
    console.log('post card result ', id)
    yield put(cardDuck.actions.setId(id))
    const result = yield call(getSocialImage)
    const action = cardDuck.actions.setResult(result.result)
    debugger
    yield put(action)

    yield put(RR.push(nav.endResult))
    //    yield put(uiDuck.actions.setShowShareCopyModal('result'))
}