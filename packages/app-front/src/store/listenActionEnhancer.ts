import {
    StoreEnhancer,
    StoreEnhancerStoreCreator,
    Reducer,
    AnyAction,
    Action, ActionCreator, Unsubscribe, Dispatch, Store
} from 'redux'
import {ActionCreatorFactory, FactoryAction, FactoryAnyAction} from '@sha/fsa'

export type Listener<P, A extends FactoryAction<P>> = (...args: ListenerArgs<P, A>) => void

export type ListenerArgs<P, A extends FactoryAction<P>> =  [A] | [A, any]

export type ActionPredicate<P, A extends FactoryAction<P>> = (action: FactoryAnyAction) =>
    action is A


export const listenActionEnhancer: StoreEnhancer =
    (createStore: StoreEnhancerStoreCreator): StoreEnhancerStoreCreator =>
        (reducer: Reducer<any, any>, preloadedState?: any) => {
            let currentListeners: Listener<any,any>[] | null = []
            let currentPredicates: ActionPredicate<FactoryAnyAction, any>[] = []
            let nextListeners = currentListeners
            let nextPredicates = currentPredicates
            function ensureCanMutateNextListeners() {
                if (nextListeners === currentListeners) {
                    nextListeners = currentListeners.slice()
                    nextPredicates = currentPredicates.slice()
                }
            }
            const store = createStore(reducer, preloadedState);
            const dispatch = store.dispatch


            const listenAction = <P, A extends FactoryAction<P>>(
                predicate: ActionPredicate<P,A>,
                listener: Listener<P, A>
            ): Unsubscribe => {

                ensureCanMutateNextListeners()
                nextListeners.push(listener)
                nextPredicates.push(predicate)
                return () => {
                    ensureCanMutateNextListeners()
                    const index = nextListeners.indexOf(listener)
                    nextListeners.splice(index, 1)
                    currentListeners = null
                    nextPredicates.splice(index, 1)
                    currentPredicates = null
                };
            }
            const listenOnce = async <P, A extends FactoryAction<P>>(
                predicate: ActionPredicate<P, A>,
                listener?: Listener<P, A>
            ) =>
                new Promise<ListenerArgs<P, A>>( (res, rej) => {
                    const handle = (action, state) => {
                        if(predicate(action)) {
                            if (listener)
                                listener([action, state])
                            res([action, state])
                            removeHandle()
                        }
                    }
                    const removeHandle = listenAction(predicate, handle)
                })
            const newDispatch: Dispatch = <P, A extends FactoryAction<P>>(action: A, ...extraArgs: any[]) => {
                dispatch(action)
                const state = store.getState()
                const listeners = (currentListeners = nextListeners)
                const predicates = (currentPredicates = nextPredicates)
                for (let i = 0; i < listeners.length; i++) {
                    const listener = listeners[i]
                    const predicate = predicates[i]
                    if(predicate(action)) {
                        listener(action, state)
                    }
                }
            }
            return {
                ...store,
                dispatch: newDispatch,
                listenAction,
                listenOnce

            };
        };

export type ListenAction<P, A extends FactoryAction<P>> = {
    (predicate: Listener<P, A>, listener): Unsubscribe
}

export type ListenOnce<P, A extends FactoryAction<P>> = {
    (predicate: Listener<P, A>): Promise<ListenerArgs<P, A>>
}
export type ListenableStore<S, P, A extends FactoryAction<P>> = Store & {
    listenAction: ListenAction<P,A>
    listenOnce: ListenOnce<P,A>
}
export default listenActionEnhancer;