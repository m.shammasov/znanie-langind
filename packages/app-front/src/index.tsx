import React from 'react'
import ReactDOM from 'react-dom'
import LogRocket from 'logrocket';

LogRocket.init('shammasov/znanie')
import App from './App'
import * as R from 'ramda'
import {configureFrontStore} from "./store/configureStore"
import {history} from '@sha/router'
import rootSaga from "./store/rootSaga"
const div = document.getElementById('root') as HTMLDivElement
const store = configureFrontStore(undefined, history, rootSaga)



window['redux'] = store
window['R'] = R

ReactDOM.render(
    <App history={history} store={store}/>,
    div
)

