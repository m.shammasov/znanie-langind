
import React, {useState} from 'react';
import {useDispatch} from "react-redux";
import {cardDuck} from "../../store/ducks/cardDuck";
import {API_BASE} from "../../constants";
type fileState = 'notSelected' | 'selected'  | 'uploading' |'uploaded' |'error'
const  SendPhoto = () => {
  const dispatch = useDispatch()
  const [selectedFile, setSelectedFile] = useState(undefined as File);
  const [fileState, setFileState] = useState('notSelected' as fileState);
  const [savedFileName, setSavedFileName] = useState(undefined)
    const onSend = () => {
  dispatch(cardDuck.actions.setPhoto({savedName: savedFileName, fileName: selectedFile.name}))
}
    const hasWrongFormat = () =>
        false

  const changeHandler = async (event) => {
    const file: File =event.target.files[0]
    setSelectedFile(file);
      setFileState('selected')
    console.log(file)
      handleSubmission(file)
  };

  const handleSubmission = async (file) => {
      setFileState('uploading')
    const formData = new FormData();

    formData.append('image', file);
    try {
        const result = await fetch(
            'https://api.spasibozaznanie.ru/upload/',
            {
                method: 'POST',
                body: formData,
                headers: {
                    'Authorization': 'Token eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoiYWNjZXNzIiwiZXhwIjoxNjI3OTk4MzM1LCJqdGkiOiI5NzNiNTgzMGVjYjM0NTM4YjRhYTBmMDY2MzUwNjVhNiIsInVzZXJfaWQiOjZ9.KAf70HBPbnYRQ7okEJyfQ1uC4AG6ofui7ndSVtUoONc',
                }
            }
        )
            .then((response) => response.json())
        console.log(result)
        setSavedFileName(result.photo_url)
        setFileState('uploaded')
        dispatch(cardDuck.actions.setPhoto({savedName: result.photo_url, fileName: file.name}))
    }catch(e) {
        console.error(e)
        setFileState('error')
    }
  };

  let content =  <div className="text-block">
                      <strong>Загрузите ваше фото</strong>
                      <span>Загрузите файл, форматы: jpg,png</span>
                  </div>
    if(fileState === 'selected' || fileState === 'uploading')
        content = <div className="text-block">
            <strong>{selectedFile.name}</strong>
            <span>Загрузка файла...</span>

        </div>

    if(fileState === 'error')
        content = <div className="text-block">
            <strong>{selectedFile.name}</strong>
            <span>{hasWrongFormat() ? 'Файл должен быть формата jpg, png':'Ошибка загрузки файла'}</span>

        </div>

    if(fileState === 'uploaded')
        content =  <div className="text-block">
            <strong>{selectedFile.name}</strong>
            <span>Загрузите файл, форматы: jpg,png</span>
            <svg width={16} height={16} onClick={() => {setFileState('notSelected'); setSelectedFile(undefined);}}>
                <use xlinkHref="#cross" />
            </svg>
        </div>

return(
    <div className="chat__sendbox" onClick={e => { if(fileState === 'uploaded') {
        e.preventDefault()
        e.stopPropagation()
        onSend()
    }}}>
        <div className={"chat__sendbox-upload " +fileState}>
              <label htmlFor="upload" style={fileState === 'uploading' ? {backgroundColor: 'lightblue'}: {}}>

                <input type="file" name="file" id={'upload'} onChange={changeHandler} />

                  {content}
              </label>
        </div>
        {
            fileState === 'uploaded' &&

            <div className="chat__sendbox-send" onClick={onSend}>
                <button className="send-btn" type="button" role="button">
                    <svg width="21" height="21">
                        <use xlinkHref="#send"></use>
                    </svg>
                </button>
            </div>
        }
    </div>

)
}


export default SendPhoto;
