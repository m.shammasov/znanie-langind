import React, {useEffect, useRef} from "react";
import {useSelector, useStore} from "react-redux";
import {messagesDuck} from "../../../store/ducks/messagesDuck";
import BotMessage from "./BotMessage";
import AttachPhotoMessage from "./AttachPhotoMessage";
import SelectCharacterMessage from "./SelectCharacterMessage";
import TextMessage from "./TextMessage";
import {useMount, useUnmount} from "react-use";

const MessagesHistory = () => {
  const messages = useSelector(messagesDuck.selectMessages)
  const holder = useRef<HTMLDivElement>()
    const store = useStore()
    const timeoutId = useRef(undefined as any)
  useEffect(() => {
      const timeout = setTimeout( () => {
          holder.current.scrollTo({top: holder.current.scrollHeight})
          document.getElementById('chatModal').parentElement.scrollTo(0,document.getElementById('chatModal').parentElement.scrollHeight)
      }, 50)
      timeoutId.current = timeout
    return () => { clearTimeout(timeoutId.current)
      }
  }, [messages])



  return (

      <div className="chat__window" ref={holder}>
        <div className="chat__dialog">
          {
            messages.map( message => {
                    if(message.side === 'bot')
                      return <BotMessage message={message} />
              if(message.type === 'photo')
                return <AttachPhotoMessage message={message} />
              if(message.type === 'character')
                return <SelectCharacterMessage message={message} />
                else
                return <TextMessage message={message} />
                }

            )
          }
        </div>
      </div>
  );
}

export default MessagesHistory;
