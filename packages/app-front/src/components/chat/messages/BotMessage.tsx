import React from "react"
import {Message} from "../../../store/ducks/messagesDuck";

const BotMessage = ({message}: {message: Message}) => (
  <div className="message">
    <div className="message__ava">
      <img
        srcSet="/img/chat/robot-ava.png 1x, /img/chat/robot-ava@2x.png 2x"
        alt="Robot"
      />
    </div>
    <div className="message__text">
      <span>{message.text}</span>
    </div>
  </div>
);

export default BotMessage;
