import React from "react";
import {Message} from "../../../store/ducks/messagesDuck";

const AttachPhotoMessage = ({message}: {message: Message}) => (
  <div className="message mod-right">
    <div className="message__text">
      <span className="mod-photo">
        <svg width={24} height={22}>
          <use xlinkHref="#photo" />
        </svg>
         {message.text}
      </span>
    </div>
  </div>
);

export default AttachPhotoMessage;
