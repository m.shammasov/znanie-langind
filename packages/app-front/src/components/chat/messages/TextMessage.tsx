import React from "react";
import {Message} from "../../../store/ducks/messagesDuck";

const TextMessage = ({message}: {message: Message}) => (
  <div className="message mod-right">
    <div className="message__text">
      <span>{message.text}</span>
    </div>
  </div>
);

export default TextMessage;
