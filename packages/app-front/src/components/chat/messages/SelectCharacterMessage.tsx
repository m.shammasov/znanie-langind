import React from "react";
import {Message} from "../../../store/ducks/messagesDuck";

const SelectCharacterMessage = ({message}: {message: Message}) => (
  <div className="message mod-right">
    <div className="message__text mod-photo">
        <img srcSet={message.text} width="98"
             height="79"/>
    </div>
  </div>
);

export default SelectCharacterMessage;
