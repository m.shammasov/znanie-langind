import React from "react";
import {useDispatch, useSelector} from "react-redux";
import {chatActions} from "../../store/rootSaga";
import {cardDuck} from "../../store/ducks/cardDuck";


const SendText = () => {
    const dispatch = useDispatch()
    const [text, setText] = React.useState('')
    const onSend = () => {
        dispatch(cardDuck.actions.setName(text))
    }
    const onEnterPress = (e: React.KeyboardEvent) => {

        if(e.code == "Enter" ) {
            e.preventDefault();
            e.stopPropagation();
            onSend()
        }
    }
    return (

        <div className="chat__sendbox">
            <div className="chat__sendbox-textarea">
            <textarea
                autoFocus={true}
                onKeyPress={onEnterPress}
                name="chattext"
                placeholder="Введите своё имя"
                value={text}
                onChange={e => setText(e.target.value)}
                rows={1}
                style={{resize: 'none',whiteSpace: 'nowrap',
                    overflow: 'hidden' }}
            />
                <div className="chat__sendbox-send" data-component="ChatFooterSendButton">
                    <button className="send-btn" type="button" role="button" onClick={onSend} >
                        <svg width="21" height="21">
                            <use xlinkHref="#send"></use>
                        </svg>
                    </button>
                </div>
            </div>
        </div>
    );
}

export default SendText;
