import React, {useState} from "react";
import {useDispatch} from "react-redux";
import {cardDuck} from "../../store/ducks/cardDuck";
import {activities} from "../../api/activities";
import characters from "../../api/characters";
import {sleep} from "@sha/utils";

const SendCharacter = () => {
    const dispatch = useDispatch()
    const onSend = (i) => {
        dispatch(cardDuck.actions.setCharacter(i))
    }
    const onSelect = async (index) => {
        setValue(index)
        await sleep(300)
        onSend(index)
    }
    const [value, setValue] = useState(undefined)
    return (
        <div className="chat__sendbox">
            <div className="chat__sendbox-variants mod-photo">
                {
                    characters.map(({title, previewUrl,}, index) => {
                        return <button className={"variant mod-photo " + (value === index ? 'active' : '')}
                                       data-value={index}
                                       onClick={() => onSelect(index)}
                        >
                            <img srcSet={'/img/chat/'+(index+1)+'/select-avatar.png 1x, /img/chat/'+(index+1)+'/select-avatar@2x.png 2x'} width="98"
                                 height="79"/>
                            {
                                value === index &&
                                <div className="variant__check">
                                    <svg width="28" height="28">
                                        <use xlinkHref="#variant_check"></use>
                                    </svg>
                                </div>
                            }
                        </button>
                    })
                }


            </div>
        </div>

    )
}

export default SendCharacter
