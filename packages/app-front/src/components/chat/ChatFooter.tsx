import React from "react";
import SendActivity from "./SendActivity";
import {RootState} from "../../store/rootReducer";
import {useSelector} from "react-redux";
import {uiDuck, UIState} from "../../store/ducks/uiDuck";
import SendText from "./SendText";
import SendComplete from "./SendComplete";
import SendPhoto from "./SendPhoto";
import SendCharacter from "./SendCharacter";
const ChatFooter = () => {
    const state: UIState = useSelector(uiDuck.selectUI)
    if(state.turn === 'bot')
        return null

    let sender = <SendComplete />
    if(state.step === 1)
        sender = <SendText/>

    if(state.step === 2)
        sender = <SendActivity/>

    if(state.step === 3)
        sender = <SendPhoto/>

    if(state.step === 4)
        sender = <SendCharacter/>
    return (
        <div className="chat__footer">
            {
                sender
            }
        </div>
    );
}

export default ChatFooter;
