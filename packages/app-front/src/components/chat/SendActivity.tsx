import React, {useState} from "react";
import {useDispatch} from "react-redux";
import {cardDuck} from "../../store/ducks/cardDuck";
import {activities} from "../../api/activities";
import {sleep} from "@sha/utils";

const SendActivity = () => {
    const dispatch = useDispatch()
    const [text, setText] = React.useState('')

    const onSend = (i) => {
        dispatch(cardDuck.actions.setActivity(i))
    }

    const onSelect =  (index) => async ()=> {
        setValue(index)

        onSend(index)
    }

    const [value, setValue] = useState(undefined)
    return (
        <div className="chat__sendbox">
            <div className="chat__sendbox-variants" data-component="ChatFooterInputOption">
                <button className="variant" data-value="Навожу суету перед кинопремьерами" onClick={onSelect(1)}>Навожу суету перед <br/>кинопремьерами
                </button>
                <button className="variant" data-value="Яжпрограммирую"  onClick={onSelect(0)}>Яжпрограммирую</button>
                <button className="variant" data-value="Смотрю мемы"  onClick={onSelect(2)}>Смотрю мемы</button>
                <button className="variant" data-value="Повелеваю гаджетами"  onClick={onSelect(3)}>Повелеваю <br/>гаджетами</button>
                <button className="variant" data-value="Не рисуюсь, а рисую"  onClick={onSelect(4)}>Не рисуюсь, <br/>а рисую</button>
                <button className="variant" data-value="Угараю по истории" onClick={onSelect(5)}>Угараю по истории</button>
                <button className="variant" data-value="Поднимаю профит" onClick={onSelect(6)}>Поднимаю <br/>профит</button>
                <button className="variant" data-value="Преклоняюсь ЗОЖу" onClick={onSelect(7)}>Преклоняюсь <br/>ЗОЖу</button>
                <button className="variant" data-value="Повелеваю пластиком" onClick={onSelect(8)}>Повелеваю <br/>пластиком</button>
                <button className="variant" data-value="Думаю о России" onClick={onSelect(9)}>Думаю о России</button>
                <button className="variant" data-value="Веду корреспонденцию со школьных полей"  onClick={onSelect(10)}>Веду
                    корреспонденцию <br/>со школьных полей</button>
                <button className="variant" data-value="Отвечаю за вписки" onClick={onSelect(11)}>Отвечаю <br/>за вписки</button>
                <button className="variant" data-value="Амбассадорю школьные рилс" onClick={onSelect(12)}>Амбассадорю <br/>школьные рилс
                </button>
                <button className="variant" data-value="Скроллю ленту на парах"  onClick={onSelect(13)}>Скроллю ленту <br/>на парах</button>
                <button className="variant" data-value="Нравлюсь всем" onClick={onSelect(14)}>Нравлюсь всем</button>
            </div>
        </div>

    )
}

export default SendActivity;
