import React from "react";
import {useDispatch} from "react-redux";
import {cardDuck} from "../../store/ducks/cardDuck";

const SendComplete = () => {
    const dispatch = useDispatch()
    return  (
        <div className="chat__sendbox">
            <button className="btn-result" type="button" role="button" onClick={() => dispatch(cardDuck.actions.setComplete(undefined))}><strong>Забери свою номинацию!</strong></button>
        </div>
    );
}

export default SendComplete;
