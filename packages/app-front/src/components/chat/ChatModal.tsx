import React from "react";
import ChatStepsDone from "./ChatStepsDone";
import SelectCharacterMessage from "./ChatUserMessageCharacter";
import SendCharacter from "./ChatFinalMessageCharacter";
import {useLocation} from "react-use";
import ChatHistory from "./ChatWindow";
import ChatFooter from "./ChatFooter";
import {useDispatch} from "react-redux";
import {replace} from '@sha/router'
import * as RR from 'connected-react-router'
import {Modal} from 'react-bootstrap'
import MessagesHistory from "./messages/MessagesHistory";
import Steps from "./Steps";

const ChatModal = () => {
  const dispatch = useDispatch()
  const location = useLocation()
  const isOpen = location.pathname === '/start/chat'
  console.log('useLocation ', location)

  const onClose = () => {
    dispatch(RR.replace('/start/second'))
  }
  return  <Modal show={isOpen} onHide={onClose} centered dialogAs={ChatDialog} >
            <div className="chat__header">
              <div className="title">Создайте своего персонажа</div>
              <div className="desc">Выполнено ответов</div>
              <button className="chat__header-close" type="button" data-bs-dismiss="modal" aria-label="Close" onClick={onClose}>
                <svg width={19} height={19}>
                  <use xlinkHref="#cross" />
                </svg>
              </button>
            </div>
            <div className="chat__body">
              {/* steps*/}
              <Steps/>
              {/* chat-window*/}
              <MessagesHistory/>
            </div>
            <ChatFooter/>
          </Modal>
}

const ChatDialog = (props) => {
  return (<div className="modal-dialog modal-dialog-centered" id={'chatModal'}>
            <div className="chat">
              {props.children}
            </div>
          </div>
  )
}
  /*
  return (
      <div
          className={"modal fade "+ (isOpen ? "show" : '')}
          id="chat1"
          data-bs-backdrop="static"
          data-bs-keyboard="false"
          tabIndex={-1}
          aria-labelledby="chat1Label"
          aria-hidden="true"
      >
        <div className="modal-dialog modal-dialog-centered">

        </div>
      </div>
  );
}
*/
export default ChatModal;
