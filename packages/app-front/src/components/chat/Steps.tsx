import React from "react";
import {useSelector} from "react-redux";
import {uiDuck} from "../../store/ducks/uiDuck";
import {is} from "ramda";

const Steps = () => {
    const uiState = useSelector(uiDuck.selectUI)
    const step = uiState.step
    const isDone = step === 5
    return (
        <div className={"steps " + (isDone? "mod-done" : "")}>
            <div className={"step " + (step>=1? " active" : "")} />
            <div className={"step " + (step>=2? " active" : "")} />
            <div className={"step " + (step>=3? " active" : "")} />
            <div className={"step " + (step>=4? " active" : "")} />
        </div>
    );
}

export default Steps;
