import React from 'react'
import {useSelector} from "react-redux";
import {RootState} from "../../store/rootReducer";

export default () => {
    const state = useSelector((state: RootState) => state)
    return <div>{JSON.stringify(state)}</div>
}