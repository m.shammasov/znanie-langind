import React, {useState} from "react";

const NavBurger = ({children, open, onClose, isMenuVisible}) => {

  return (
      <div className={"core-nav__burger"+(open ? ' open':'')} style={{visibility: (open || isMenuVisible)? 'visible': 'hidden'}}>
        <div className="core-nav__burger-content">
          <div className="core-nav__burger-close" onClick={onClose}>
            <button className="burger-close" type="button" role="button">
              <svg
                  width={60}
                  height={59}
                  viewBox="0 0 60 59"
                  fill="none"
                  xmlns="http://www.w3.org/2000/svg"
              >
                <rect width={60} height={59} rx={13} fill="white" />
                <rect
                    x="18.2427"
                    y={13}
                    width={40}
                    height={6}
                    rx={3}
                    transform="rotate(45 18.2427 13)"
                    fill="#A4F375"
                />
                <rect
                    x={14}
                    y="41.2842"
                    width={40}
                    height={6}
                    rx={3}
                    transform="rotate(-45 14 41.2842)"
                    fill="#A4F375"
                />
              </svg>
            </button>
          </div>
          <ul onClick={onClose}>
          {
            children
          }
          </ul>
        </div>
      </div>
  );
}

export default NavBurger;
