import React, {useState} from "react";
import NavBurger from "./NavBurger";
import NavTopMenu from "./NavTopMenu";
import {NavLink} from "react-router-dom";
import nav from "../../nav";

const NavMenu = ({renderMenu, isMenuVisible}) => {
    const [open, setOpen] = useState(false)
    const onClose = () => {
        setOpen(false)
    }
    const onOpen = ()=> [
        setOpen(true)
    ]
    return (
        <nav className="core-nav" >
            <NavBurger onClose={onClose} isMenuVisible={isMenuVisible} open={open} >
                {
                    renderMenu()
                }
            </NavBurger>
            <NavTopMenu onOpen={onOpen} isMenuVisible={isMenuVisible} >
                {
                    renderMenu()
                }
            </NavTopMenu>
        </nav>
    );
}

export default NavMenu;
