import React, {useState} from "react";
import {Link, NavLink} from "react-router-dom";
import nav from "../../nav";

const NavTopMenu = ({children, onOpen,isMenuVisible}) => {

    return (
        <div className="core-nav__content container-fluid">
            <div className="core-nav__logo">
                <a className="logo" href="https://premiya.znanierussia.ru/" target={'_blank'}>
                    <img
                        src="/img/logo-mini.svg"
                        alt="\u041F\u0440\u0438\u0417\u043D\u0430\u043D\u0438\u0435"
                    />
                    <img
                        src="/img/logo.svg"
                        alt="\u041F\u0440\u0438\u0417\u043D\u0430\u043D\u0438\u0435"
                    />
                </a>
            </div>
            <div className="core-nav__burger-btn" style={{visibility: (open || isMenuVisible)? 'visible': 'hidden'}}>
                <button className="burger-btn" type="button" role="button" onClick={onOpen}>
                    <svg
                        width={60}
                        height={59}
                        viewBox="0 0 60 59"
                        fill="none"
                        xmlns="http://www.w3.org/2000/svg"
                    >
                        <rect width={60} height={59} rx={13} fill="#A4F375" />
                        <rect x={10} y={14} width={40} height={6} rx={3} fill="white" />
                        <rect x={10} y={26} width={40} height={6} rx={3} fill="white" />
                        <rect x={10} y={38} width={40} height={6} rx={3} fill="white" />
                    </svg>
                </button>
            </div>
            <div className="core-nav__links" style={{visibility: (isMenuVisible)? 'visible': 'hidden'}}>
                <ul>
                    {children}
                </ul>
            </div>
        </div>
    );
}

export default NavTopMenu;
