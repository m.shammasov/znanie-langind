import React, {useEffect, useRef, useState} from "react";
import * as R from 'ramda'
/*import {SequenceAnimator} from 'react-sequence-animator'

const getIndex = i =>
    i < 10 ? '0'+i : i.toString()

const useAnimationFrame = callback => {
    // Use useRef for mutable variables that we want to persist
    // without triggering a re-render on their change
    const requestRef = React.useRef();
    const previousTimeRef = React.useRef();

    const animate = time => {
        if (previousTimeRef.current != undefined) {
            const deltaTime = time - previousTimeRef.current;
            callback(deltaTime)
        }
        previousTimeRef.current = time;
        requestRef.current = requestAnimationFrame(animate);
    }

    React.useEffect(() => {
        requestRef.current = requestAnimationFrame(animate);
        return () => cancelAnimationFrame(requestRef.current);
    }, []); // Make sure the effect runs only once
}

const IntroAnimation = () => {

    const [frame, setFrame] = useState(0)

    const imgSources = R.times( (i) =>
        <picture className={i === Math.floor(frame) ? 'active' : ''} >
            <source srcSet={`/img/animation/4_00${getIndex(i)}.webp`} type="image/webp" />
            <img src={`/img/animation/4_00${getIndex(i)}.png`}  />
        </picture>,
        91
    )
    const frameRef = useRef(0)
    const dirRef = useRef(1)

    useEffect( () => {
        const id = setInterval(() => {
            let currentFrame = frameRef.current
            if(currentFrame === 90) {
                dirRef.current = -1
            } else if(currentFrame === 0)
                dirRef.current = 1
            const next =  currentFrame+dirRef.current;
            setFrame(next)
            frameRef.current = next
        }, 45)
        return () => {clearInterval(id)}
    }, [])
   /* useAnimationFrame(() => {
        let currentFrame = frameRef.current
        if(currentFrame === 90) {
            currentFrame = -1
        }
        const next =  currentFrame+1;
        setFrame(next)
        frameRef.current = next
    })
    return (
        <figure className="figure" id="animation">
            {imgSources}
        </figure>
    );

}*/
const IntroAnimation = () => (
  <figure className="figure" id="animation">
    <picture >
      <source srcSet="/img/animation/4_0001.webp" type="image/webp" />
      <img src="/img/animation/4_0001.png"  />
    </picture>
    <picture>
      <source srcSet="/img/animation/4_0002.webp" type="image/webp" />
      <img src="/img/animation/4_0002.png"  />
    </picture>
    <picture>
      <source srcSet="/img/animation/4_0003.webp" type="image/webp" />
      <img src="/img/animation/4_0003.png"  />
    </picture>
    <picture>
      <source srcSet="/img/animation/4_0004.webp" type="image/webp" />
      <img src="/img/animation/4_0004.png"  />
    </picture>
    <picture>
      <source srcSet="/img/animation/4_0005.webp" type="image/webp" />
      <img src="/img/animation/4_0005.png"  />
    </picture>
    <picture>
      <source srcSet="/img/animation/4_0006.webp" type="image/webp" />
      <img src="/img/animation/4_0006.png"  />
    </picture>
    <picture>
      <source srcSet="/img/animation/4_0007.webp" type="image/webp" />
      <img src="/img/animation/4_0007.png"  />
    </picture>
    <picture>
      <source srcSet="/img/animation/4_0008.webp" type="image/webp" />
      <img src="/img/animation/4_0008.png"  />
    </picture>
    <picture>
      <source srcSet="/img/animation/4_0009.webp" type="image/webp" />
      <img src="/img/animation/4_0009.png"  />
    </picture>
    <picture>
      <source srcSet="/img/animation/4_0010.webp" type="image/webp" />
      <img src="/img/animation/4_0010.png"  />
    </picture>
    <picture>
      <source srcSet="/img/animation/4_0011.webp" type="image/webp" />
      <img src="/img/animation/4_0011.png"  />
    </picture>
    <picture>
      <source srcSet="/img/animation/4_0012.webp" type="image/webp" />
      <img src="/img/animation/4_0012.png"  />
    </picture>
    <picture>
      <source srcSet="/img/animation/4_0013.webp" type="image/webp" />
      <img src="/img/animation/4_0013.png"  />
    </picture>
    <picture>
      <source srcSet="/img/animation/4_0014.webp" type="image/webp" />
      <img src="/img/animation/4_0014.png"  />
    </picture>
    <picture>
      <source srcSet="/img/animation/4_0015.webp" type="image/webp" />
      <img src="/img/animation/4_0015.png"  />
    </picture>
    <picture>
      <source srcSet="/img/animation/4_0016.webp" type="image/webp" />
      <img src="/img/animation/4_0016.png"  />
    </picture>
    <picture>
      <source srcSet="/img/animation/4_0017.webp" type="image/webp" />
      <img src="/img/animation/4_0017.png"  />
    </picture>
    <picture>
      <source srcSet="/img/animation/4_0018.webp" type="image/webp" />
      <img src="/img/animation/4_0018.png"  />
    </picture>
    <picture>
      <source srcSet="/img/animation/4_0019.webp" type="image/webp" />
      <img src="/img/animation/4_0019.png"  />
    </picture>
    <picture>
      <source srcSet="/img/animation/4_0020.webp" type="image/webp" />
      <img src="/img/animation/4_0020.png"  />
    </picture>
    <picture>
      <source srcSet="/img/animation/4_0021.webp" type="image/webp" />
      <img src="/img/animation/4_0021.png"  />
    </picture>
    <picture>
      <source srcSet="/img/animation/4_0022.webp" type="image/webp" />
      <img src="/img/animation/4_0022.png"  />
    </picture>
    <picture>
      <source srcSet="/img/animation/4_0023.webp" type="image/webp" />
      <img src="/img/animation/4_0023.png"  />
    </picture>
    <picture>
      <source srcSet="/img/animation/4_0024.webp" type="image/webp" />
      <img src="/img/animation/4_0024.png"  />
    </picture>
    <picture>
      <source srcSet="/img/animation/4_0025.webp" type="image/webp" />
      <img src="/img/animation/4_0025.png"  />
    </picture>
    <picture>
      <source srcSet="/img/animation/4_0026.webp" type="image/webp" />
      <img src="/img/animation/4_0026.png"  />
    </picture>
    <picture>
      <source srcSet="/img/animation/4_0027.webp" type="image/webp" />
      <img src="/img/animation/4_0027.png"  />
    </picture>
    <picture>
      <source srcSet="/img/animation/4_0028.webp" type="image/webp" />
      <img src="/img/animation/4_0028.png"  />
    </picture>
    <picture>
      <source srcSet="/img/animation/4_0029.webp" type="image/webp" />
      <img src="/img/animation/4_0029.png"  />
    </picture>
    <picture>
      <source srcSet="/img/animation/4_0030.webp" type="image/webp" />
      <img src="/img/animation/4_0030.png"  />
    </picture>
    <picture>
      <source srcSet="/img/animation/4_0031.webp" type="image/webp" />
      <img src="/img/animation/4_0031.png"  />
    </picture>
    <picture>
      <source srcSet="/img/animation/4_0032.webp" type="image/webp" />
      <img src="/img/animation/4_0032.png"  />
    </picture>
    <picture>
      <source srcSet="/img/animation/4_0033.webp" type="image/webp" />
      <img src="/img/animation/4_0033.png"  />
    </picture>
    <picture>
      <source srcSet="/img/animation/4_0034.webp" type="image/webp" />
      <img src="/img/animation/4_0034.png"  />
    </picture>
    <picture>
      <source srcSet="/img/animation/4_0035.webp" type="image/webp" />
      <img src="/img/animation/4_0035.png"  />
    </picture>
    <picture>
      <source srcSet="/img/animation/4_0036.webp" type="image/webp" />
      <img src="/img/animation/4_0036.png"  />
    </picture>
    <picture>
      <source srcSet="/img/animation/4_0037.webp" type="image/webp" />
      <img src="/img/animation/4_0037.png"  />
    </picture>
    <picture>
      <source srcSet="/img/animation/4_0038.webp" type="image/webp" />
      <img src="/img/animation/4_0038.png"  />
    </picture>
    <picture>
      <source srcSet="/img/animation/4_0039.webp" type="image/webp" />
      <img src="/img/animation/4_0039.png"  />
    </picture>
    <picture>
      <source srcSet="/img/animation/4_0040.webp" type="image/webp" />
      <img src="/img/animation/4_0040.png"  />
    </picture>
    <picture>
      <source srcSet="/img/animation/4_0041.webp" type="image/webp" />
      <img src="/img/animation/4_0041.png"  />
    </picture>
    <picture>
      <source srcSet="/img/animation/4_0042.webp" type="image/webp" />
      <img src="/img/animation/4_0042.png"  />
    </picture>
    <picture>
      <source srcSet="/img/animation/4_0043.webp" type="image/webp" />
      <img src="/img/animation/4_0043.png"  />
    </picture>
    <picture>
      <source srcSet="/img/animation/4_0044.webp" type="image/webp" />
      <img src="/img/animation/4_0044.png"  />
    </picture>
    <picture>
      <source srcSet="/img/animation/4_0045.webp" type="image/webp" />
      <img src="/img/animation/4_0045.png"  />
    </picture>
    <picture>
      <source srcSet="/img/animation/4_0046.webp" type="image/webp" />
      <img src="/img/animation/4_0046.png"  />
    </picture>
    <picture>
      <source srcSet="/img/animation/4_0047.webp" type="image/webp" />
      <img src="/img/animation/4_0047.png"  />
    </picture>
    <picture>
      <source srcSet="/img/animation/4_0048.webp" type="image/webp" />
      <img src="/img/animation/4_0048.png"  />
    </picture>
    <picture>
      <source srcSet="/img/animation/4_0049.webp" type="image/webp" />
      <img src="/img/animation/4_0049.png"  />
    </picture>
    <picture>
      <source srcSet="/img/animation/4_0050.webp" type="image/webp" />
      <img src="/img/animation/4_0050.png"  />
    </picture>
    <picture>
      <source srcSet="/img/animation/4_0051.webp" type="image/webp" />
      <img src="/img/animation/4_0051.png"  />
    </picture>
    <picture>
      <source srcSet="/img/animation/4_0052.webp" type="image/webp" />
      <img src="/img/animation/4_0052.png"  />
    </picture>
    <picture>
      <source srcSet="/img/animation/4_0053.webp" type="image/webp" />
      <img src="/img/animation/4_0053.png"  />
    </picture>
    <picture>
      <source srcSet="/img/animation/4_0054.webp" type="image/webp" />
      <img src="/img/animation/4_0054.png"  />
    </picture>
    <picture>
      <source srcSet="/img/animation/4_0055.webp" type="image/webp" />
      <img src="/img/animation/4_0055.png"  />
    </picture>
    <picture>
      <source srcSet="/img/animation/4_0056.webp" type="image/webp" />
      <img src="/img/animation/4_0056.png"  />
    </picture>
    <picture>
      <source srcSet="/img/animation/4_0057.webp" type="image/webp" />
      <img src="/img/animation/4_0057.png"  />
    </picture>
    <picture>
      <source srcSet="/img/animation/4_0058.webp" type="image/webp" />
      <img src="/img/animation/4_0058.png"  />
    </picture>
    <picture>
      <source srcSet="/img/animation/4_0059.webp" type="image/webp" />
      <img src="/img/animation/4_0059.png"  />
    </picture>
    <picture>
      <source srcSet="/img/animation/4_0060.webp" type="image/webp" />
      <img src="/img/animation/4_0060.png"  />
    </picture>
    <picture>
      <source srcSet="/img/animation/4_0061.webp" type="image/webp" />
      <img src="/img/animation/4_0061.png"  />
    </picture>
    <picture>
      <source srcSet="/img/animation/4_0062.webp" type="image/webp" />
      <img src="/img/animation/4_0062.png"  />
    </picture>
    <picture>
      <source srcSet="/img/animation/4_0063.webp" type="image/webp" />
      <img src="/img/animation/4_0063.png"  />
    </picture>
    <picture>
      <source srcSet="/img/animation/4_0064.webp" type="image/webp" />
      <img src="/img/animation/4_0064.png"  />
    </picture>
    <picture>
      <source srcSet="/img/animation/4_0065.webp" type="image/webp" />
      <img src="/img/animation/4_0065.png"  />
    </picture>
    <picture>
      <source srcSet="/img/animation/4_0066.webp" type="image/webp" />
      <img src="/img/animation/4_0066.png"  />
    </picture>
    <picture>
      <source srcSet="/img/animation/4_0067.webp" type="image/webp" />
      <img src="/img/animation/4_0067.png"  />
    </picture>
    <picture>
      <source srcSet="/img/animation/4_0068.webp" type="image/webp" />
      <img src="/img/animation/4_0068.png"  />
    </picture>
    <picture>
      <source srcSet="/img/animation/4_0069.webp" type="image/webp" />
      <img src="/img/animation/4_0069.png"  />
    </picture>
    <picture>
      <source srcSet="/img/animation/4_0070.webp" type="image/webp" />
      <img src="/img/animation/4_0070.png"  />
    </picture>
    <picture>
      <source srcSet="/img/animation/4_0071.webp" type="image/webp" />
      <img src="/img/animation/4_0071.png"  />
    </picture>
    <picture>
      <source srcSet="/img/animation/4_0072.webp" type="image/webp" />
      <img src="/img/animation/4_0072.png"  />
    </picture>
    <picture>
      <source srcSet="/img/animation/4_0073.webp" type="image/webp" />
      <img src="/img/animation/4_0073.png"  />
    </picture>
    <picture>
      <source srcSet="/img/animation/4_0074.webp" type="image/webp" />
      <img src="/img/animation/4_0074.png"  />
    </picture>
    <picture>
      <source srcSet="/img/animation/4_0075.webp" type="image/webp" />
      <img src="/img/animation/4_0075.png"  />
    </picture>
    <picture>
      <source srcSet="/img/animation/4_0076.webp" type="image/webp" />
      <img src="/img/animation/4_0076.png"  />
    </picture>
    <picture>
      <source srcSet="/img/animation/4_0077.webp" type="image/webp" />
      <img src="/img/animation/4_0077.png"  />
    </picture>
    <picture>
      <source srcSet="/img/animation/4_0078.webp" type="image/webp" />
      <img src="/img/animation/4_0078.png"  />
    </picture>
    <picture>
      <source srcSet="/img/animation/4_0079.webp" type="image/webp" />
      <img src="/img/animation/4_0079.png"  />
    </picture>
    <picture>
      <source srcSet="/img/animation/4_0080.webp" type="image/webp" />
      <img src="/img/animation/4_0080.png"  />
    </picture>
    <picture>
      <source srcSet="/img/animation/4_0081.webp" type="image/webp" />
      <img src="/img/animation/4_0081.png"  />
    </picture>
    <picture>
      <source srcSet="/img/animation/4_0082.webp" type="image/webp" />
      <img src="/img/animation/4_0082.png"  />
    </picture>
    <picture>
      <source srcSet="/img/animation/4_0083.webp" type="image/webp" />
      <img src="/img/animation/4_0083.png"  />
    </picture>
    <picture>
      <source srcSet="/img/animation/4_0084.webp" type="image/webp" />
      <img src="/img/animation/4_0084.png"  />
    </picture>
    <picture>
      <source srcSet="/img/animation/4_0085.webp" type="image/webp" />
      <img src="/img/animation/4_0085.png"  />
    </picture>
    <picture>
      <source srcSet="/img/animation/4_0086.webp" type="image/webp" />
      <img src="/img/animation/4_0086.png"  />
    </picture>
    <picture>
      <source srcSet="/img/animation/4_0087.webp" type="image/webp" />
      <img src="/img/animation/4_0087.png"  />
    </picture>
    <picture>
      <source srcSet="/img/animation/4_0088.webp" type="image/webp" />
      <img src="/img/animation/4_0088.png"  />
    </picture>
    <picture>
      <source srcSet="/img/animation/4_0089.webp" type="image/webp" />
      <img src="/img/animation/4_0089.png"  />
    </picture>
    <picture>
      <source srcSet="/img/animation/4_0090.webp" type="image/webp" />
      <img src="/img/animation/4_0090.png"  />
    </picture>
  </figure>
);

export default IntroAnimation;
