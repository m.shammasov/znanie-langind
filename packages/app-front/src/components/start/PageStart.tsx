import React, {useEffect, useState} from "react";
import NavMenu from "../nav/NavMenu";
import StartFirstScreen from "./StartFirstScreen";
import StartSecondScreen from "./StartSecondScreen";
import ChatModal from "../chat/ChatModal";
import {useLocation} from "react-use";
import {createScrollStopListener} from "../../createScrollStopListener";
import {useDispatch} from "react-redux";
import * as RR from 'connected-react-router'
import nav from "../../nav";
import {NavLink} from "react-router-dom";
const PageStart = () => {
    const location = useLocation()
    const dispatch = useDispatch()

    const {pathname} = location

    useEffect(() => {
        console.log('loc', location)
        if(pathname.endsWith('first'))
            window.scrollTo(0, 0);
        else if(pathname.endsWith('chat') || pathname.endsWith('second'))
            document.getElementById('second').scrollIntoView()

    }, [pathname]);
    const [isMenuVisible, setIsMenuVisible] = useState(true)
    React.useEffect(() => {
        let isMenuVisible = true
        const checkMenu = () => {
            const html =  document.getElementsByTagName('html')[0]
            const {scrollTop, scrollHeight, clientHeight} = html
            const scrollMaxTop = scrollHeight - clientHeight

            const newIsMenuVisible = scrollTop < 30 || scrollTop > scrollMaxTop - 30
            if(newIsMenuVisible !== isMenuVisible)
            {
                isMenuVisible = newIsMenuVisible
                setIsMenuVisible(isMenuVisible)
            }
        }
        const id = setInterval(checkMenu, 30)
        return () => {

            clearInterval(id)
        }
    }, [])

    React.useEffect(() => {
        if(window.innerWidth > 576) {
            const destroyListener = createScrollStopListener(document, (e) => {
                const html = document.getElementsByTagName('html')[0]
                const {scrollTop, scrollHeight, clientHeight} = html
                const scrollMaxTop = scrollHeight - clientHeight
                console.log('onscrollstop', scrollTop, scrollMaxTop / 2, scrollMaxTop,)

                if (!window.location.pathname.endsWith('chat')) {
                    if (scrollTop > scrollMaxTop / 2) {
                        dispatch(RR.push(nav.startSecond))
                    } else if (scrollTop < scrollMaxTop / 2) {
                        dispatch(RR.push(nav.startFirst))

                    }
                }
            });
            return () => destroyListener(); // when App component is unmounted
        }
    }, []);
    return (
        <>
            <NavMenu
                isMenuVisible={isMenuVisible}
                renderMenu={
                    () => <>
                        <li>
                            <NavLink to={nav.startFirst}>Главная
                            </NavLink>
                        </li>
                        <li>

                            <NavLink to={nav.startSecond}>Премия «Знание»</NavLink>
                        </li>
                        <li>

                            <NavLink to={nav.startChat}>Вызвать чат-бота</NavLink>
                        </li>
                    </>
                }
            >

            </NavMenu>
            <StartFirstScreen/>
            <StartSecondScreen/>
            <ChatModal/>
        </>

    );
}

export default PageStart;
