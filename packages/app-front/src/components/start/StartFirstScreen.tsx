import React from "react";
import IntroAnimation from "./IntroAnimation";

const StartFirstScreen = () => (
  <div className="b-main" id={'first'}>
    <div className="mobile-figure">
      <img srcSet="/img/mobile-figure.png 1x, /img/mobile-figure@2x.png 2x"  />
    </div>
    <IntroAnimation></IntroAnimation>
    <div className="container-fluid">
      <div className="block-figure">
        <div className="block-figure__box" />
        <div className="block-figure__content">
          <div className="block-figure__content-decor" />
          <div className="titles">
            <h1 className="title">
              Социальная <br />
              премия <br />
              <span>“ПриЗнание“</span>
            </h1>
            <div className="desc">
              <span>
                Ты такой, как ты, и тебе точно есть, чем гордиться – ведь каждый
                день ты делишься знаниями с другими 📖 Например, выкладываешь
                фотографии, шеришь с друзьями интересные факты и мемы, заливаешь
                в Интернет собственные классные видео 😁 Это же действительно
                круто, правда 🔥 Хватит думать, что ты ни на что не влияешь!
                Получи свое «ПриЗнание» и никогда не забывай —
                #CМеняНачинаетсяЗнание.
              </span>
            </div>
          </div>
          <a className="scroll-btn" href="#second">
            <svg
              width={59}
              height={59}
              viewBox="0 0 59 59"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <rect width={59} height={59} rx={17} fill="currentcolor" />
              <path d="M45 21H15V27.8478L30 42L45 27.8478V21Z" fill="white" />
            </svg>
          </a>
          <div className="hashtag">
            <a >#сменяначинается</a>
          </div>
        </div>
        <div className="block-figure__top-decor">
          <img
            srcSet="/img/purple-figure-small.png 1x, /img/purple-figure-small@2x.png 2x"
            alt
          />
        </div>
        <div className="block-figure__bot-decor">
          <img
            srcSet="/img/purple-figure.png 1x, /img/purple-figure@2x.png 2x"
            alt
          />
        </div>
      </div>
    </div>
  </div>
);

export default StartFirstScreen;
