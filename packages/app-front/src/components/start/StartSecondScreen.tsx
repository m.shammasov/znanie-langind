import React from "react"
import {NavLink} from "react-router-dom"
import nav from "../../nav"
import {useDispatch} from "react-redux";
import * as RR from 'connected-react-router'
const StartSecondScreen = () => {

  const dispatch = useDispatch()

  const onBotClick = () => {
    dispatch(RR.push(nav.startChat))
  }

  return (
      <div className="b-second" id="second">
        <div className="container-fluid">
          <div className="robot-box">
            <div className="robot-box__figure" onClick={onBotClick}>
              <figure>
                <img
                    srcSet="/img/second/robot-v.png 1x, /img/second/robot-v@2x.png 2x"
                />
              </figure>
            </div>
            <div className="robot-box__hashtag">
              <a>#сменяначинается</a>
            </div>
            <div className="robot-box__title">
              <h1 className="title">Совсем скоро</h1>
            </div>
            <div className="robot-box__content">
              <p>
                начнется онлайн-голосование <a target={'_blank'} href={'https://premiya.znanierussia.ru?utm_source=LandingPage&utm_medium=SpecialProject&utm_campaign=Premiya'}>на сайте Премии «Знание»</a>,
                которую получают люди и проекты в сфере просвещения! Хочешь узнать подробнее?
              </p>
              <div className="robot-box__cta">
                <NavLink to={nav.startChat} type="button" data-bs-toggle="modal" data-bs-target="#chat1">
                  Попробовать
                </NavLink>
              </div>
            </div>
            {}
          </div>
        </div>
      </div>
  );
}

export default StartSecondScreen;
