import React, {useEffect, useRef, useState} from "react"
import EndResultScreen from "./EndResultScreen"
import EndNominationScreen from "./EndNominationScreen"
import EndSupportScreen from "./EndSupportScreen"
import NavMenu from "../nav/NavMenu"
import {Link, NavLink} from "react-router-dom";
import nav from "../../nav";
import {useLocation, useScroll} from "react-use";
import {useDispatch} from "react-redux";
import {createScrollStopListener} from "../../createScrollStopListener";
import * as RR from "connected-react-router";
const html = document.getElementsByTagName('html')[0]
const PageEnd = () => {
    const location = useLocation()
    const dispatch = useDispatch()

    const {pathname} = location

    const isShare = pathname.includes('share')
    const ref = useRef(html)
    useEffect(() => {
        console.log('loc', location)
        if(pathname.endsWith(nav.endResult))
            window.scrollTo(0, 0);
        else if(pathname.endsWith(nav.endNomination))
            document.getElementById('nomination').scrollIntoView()
        else if(pathname.endsWith(nav.endSupport))
            document.getElementById('support').scrollIntoView()

    }, [pathname]);

    const [isMenuVisible, setIsMenuVisible] = useState(true)
    React.useEffect(() => {
        let isMenuVisible = true
        const checkMenu = () => {
            const html =  document.getElementsByTagName('html')[0]
            const {scrollTop, scrollHeight, clientHeight} = html
            const scrollMaxTop = scrollHeight - clientHeight
            const half = scrollMaxTop /2
            const newIsMenuVisible = scrollTop < 30 || scrollTop > scrollMaxTop - 30 || (scrollTop > half - 30 && scrollTop < half + 30)
            if(newIsMenuVisible !== isMenuVisible)
            {
                isMenuVisible = newIsMenuVisible
                setIsMenuVisible(isMenuVisible)
            }
        }

        const id = setInterval(checkMenu, 30)
        return () => {

            clearInterval(id)
        }
    }, [])

    React.useEffect(() => {
        if(window.innerWidth > 576) {
            const destroyListener = createScrollStopListener(document, (e) => {
                const html = document.getElementsByTagName('html')[0]
                const {scrollTop, scrollHeight, clientHeight} = html
                const scrollMaxTop = scrollHeight - clientHeight
                console.log('onscrollstop', scrollTop, scrollMaxTop / 2, scrollMaxTop,)

                if (scrollTop < scrollMaxTop / 3) {
                    dispatch(RR.push(nav.endResult))
                } else if (scrollTop < scrollMaxTop / 3 * 2) {
                    dispatch(RR.push(nav.endNomination))

                } else {
                    dispatch(RR.push(nav.endSupport))
                }
            });
            return () => destroyListener(); // when App component is unmounted
        }
    }, []);
    return (
        <>
            <NavMenu
                isMenuVisible={isMenuVisible}
                renderMenu={() =>
                <ul>
                    <li>
                        <Link to={nav.startFirst}>Главная</Link>
                    </li>
                    <li>
                        <NavLink to={nav.endResult}>Награда</NavLink>
                    </li>
                    <li>
                        <NavLink to={nav.endNomination}>Премия</NavLink>
                    </li>
                    <li>
                        <NavLink to={nav.endSupport}>
                            Поддержать участников
                        </NavLink>
                    </li>
                </ul>
            }></NavMenu>
            <EndResultScreen></EndResultScreen>
            <EndNominationScreen/>
            <EndSupportScreen/>
        </>
    );
}

export default PageEnd;
