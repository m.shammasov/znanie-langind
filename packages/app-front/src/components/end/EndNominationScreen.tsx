import React from "react";

const EndNominationScreen = () => (
  <div className="b-nomination" id={'nomination'}>
    <div className="container-fluid">
      <div className="nomination">
        <div className="nomination__left">
          <div className="nomination__top">
            <div className="nomination__top-hashtag">
              <a >#сменяначинается</a>
            </div>
            <h1 className="title">Свою номинацию ты получил</h1>
          </div>
          <div className="nomination__content">
            <p>
              Не забудь поддержать просветителей, которые вносят вклад в
              образование прямо сейчас – запускают проекты и ведут активную
              просветительскую деятельность. 29 ноября можно будет проголосовать
              за образовательные и научные проекты премии «Знание».
            </p>
          </div>
        </div>
        <div className="nomination__right">
          <div className="nomination__right-arrow">
            <svg
              width={15}
              height={38}
              viewBox="0 0 15 38"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <rect x="6.44629" width="1.35714" height="31.5536" fill="black" />
              <path
                d="M0 25.907L1.368 25.1172L7.06699 34.9881L7.06701 37.9083L0 25.907Z"
                fill="black"
              />
              <path
                d="M12.7803 25.074L14.1483 25.8639L6.98256 37.9085L7.08128 34.945L12.7803 25.074Z"
                fill="black"
              />
            </svg>
          </div>
          <div className="nomination__right-logo">
            <svg
              width={59}
              height={59}
              viewBox="0 0 59 59"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
            >
              <path
                d="M59 41.7917V17.2083C59 12.6444 57.187 8.26739 53.9598 5.0402C50.7326 1.81301 46.3556 0 41.7917 0H17.2083C12.6444 0 8.2674 1.81301 5.04021 5.0402C1.81302 8.26739 0 12.6444 0 17.2083V41.7917C0.00168448 46.0395 1.5744 50.1365 4.41546 53.2944C7.25653 56.4522 11.1652 58.4477 15.3892 58.8968L33.4333 44.25H14.0912V37.1995L33.4333 21.4957H14.8434V14.75H44.9088V21.8005L25.5667 37.5043H44.8941V44.5548L27.1351 59H41.7917C46.3556 59 50.7326 57.187 53.9598 53.9598C57.187 50.7326 59 46.3556 59 41.7917Z"
                fill="#A4F375"
              />
            </svg>
          </div>
          <div className="nomination__right-cta">
            <a href="https://premiya.znanierussia.ru/?utm_source=LandingPage&utm_medium=SpecialProject&utm_campaign=Premiya" target={'_blank'}>Следи за премией</a>
          </div>
        </div>
      </div>
    </div>
  </div>
);

export default EndNominationScreen;
