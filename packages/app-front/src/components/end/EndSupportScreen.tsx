import React from "react";
import {useDispatch, useSelector} from "react-redux";
import {uiDuck} from "../../store/ducks/uiDuck";
import {cardDuck} from "../../store/ducks/cardDuck";
import copy from "copy-to-clipboard";

const EndSupportScreen = () => {
  const dispatch  =useDispatch()

  const onShare = () => {
    dispatch(uiDuck.actions.setShowShareCopyModal('main'))
  }

  const onMore = () => {
    window.open('https://premiya.znanierussia.ru/?utm_source=LandingPage&utm_medium=SpecialProject&utm_campaign=Premiya','_blank')
  }

  return (
      <div className="b-support" id={'support'}>
        <div className="container-fluid">
          <div className="support">
            <div className="support__left">
                 {
                 //<div className="support__timer"><span>01&nbsp;:&nbsp;53</span></div>
                 }
                 <div className="support__top">
                <h1 className="title">Премия</h1>
              </div>
              <div className="support__content">
                <p>«Знание» – уникальная всероссийская премия, которую может получить любой человек или организация, занимающаяся просвещением, – школьники, студенты, молодые ученые, преподаватели, СМИ, компании и даже блогеры.</p>
                <ul>
                  <li>– 15 номинаций, затрагивающих разные области жизни</li>
                  <li>– номинант – человек-просветитель или просветительский проект</li>
                  <li>– почетное жюри</li>
                  <li>– дополнительный приз зрительских симпатий</li>
                </ul>
                <p>Следи за наградами в образовательных и научных проектах премии «Знание».</p>
              </div>
            </div>
            <div className="support__right">
              <figure><img className="lazy entered loaded" data-srcset="/img/support/box.png 1x, /img/support/box@2x.png 2x" data-ll-status="loaded" srcSet="/img/support/box.png 1x, /img/support/box@2x.png 2x" /></figure>
            </div>
            <div className="support__bottom">
              <div className="support__bottom-share hand"><a onClick={onShare}>Поделиться</a></div>
              <div className="support__bottom-green">
                <h3 className="title">Оцени премию</h3>
                <div className="btn-box" onClick={onMore}>
                  <button className="btn hand" type="button">Узнать о премии</button>
                  <figure>
                    <svg width={48} height={19} viewBox="0 0 48 19" fill="none" xmlns="http://www.w3.org/2000/svg">
                      <rect y="10.0886" width="1.68121" height="39.0882" transform="rotate(-90 0 10.0886)" fill="black" />
                      <path d="M32.0933 18.0742L31.1148 16.3796L43.3429 9.31971L46.9603 9.3197L32.0933 18.0742Z" fill="black" />
                      <path d="M31.0615 2.24219L32.0399 0.547525L46.9608 9.42432L43.2895 9.30203L31.0615 2.24219Z" fill="black" />
                    </svg>
                  </figure>
                </div>
              </div>
            </div>
          </div>
        </div>

      </div>
  );
}

export default EndSupportScreen;
