import React, {useEffect, useState} from 'react'
import {useDispatch, useSelector} from "react-redux";
import {useLocation, useMount} from "react-use";
import * as RR from "connected-react-router";
import {Modal} from 'react-bootstrap'
import PolicyModal from "./PolicyModal";
import {uiDuck} from "../../store/ducks/uiDuck";
import {cardDuck} from "../../store/ducks/cardDuck";
import copy from "copy-to-clipboard";

export default () => {
    const dispatch = useDispatch()
    const uiState = useSelector(uiDuck.selectUI)

    const onClose = () => {
        dispatch(uiDuck.actions.setShowShareOkModal(false))
    }

    return <Modal show={uiState.showShareOkModal} onHide={onClose} centered dialogAs= {() =>
            <div className="modal-dialog modal-dialog-centered">
                <div className="share-copy mod-ok">
                    <div className="share-copy__ok">
                        <div className="share-copy__ok-bg"><img src="/img/share/share-figure.svg" alt=""/></div>
                        <div className="share-copy__ok-content">
                            <button className="chat__header-close" onClick={onClose} type="button" data-bs-dismiss="modal" aria-label="Close">
                                <svg width={19} height={19}>
                                    <use xlinkHref="#cross"/>
                                </svg>
                            </button>
                            <h4 className="title"><strong>Вау! </strong>Скорее делай репост и покажи друзьям, кто тут круче всех
                            </h4>
                            <div className="ribbon">Вы поделились ссылкой!</div>
                            <button className="btn-ok" type="button" data-bs-dismiss="modal" aria-label="Close" onClick={onClose}>Ок</button>
                        </div>
                    </div>
                </div>
            </div>
        }>
    </Modal>

}

const ShareOkDialog = (props) => {
    return (<div className="modal-dialog modal-dialog-centered">
            {}
            </div>


    )
}