import React, {useEffect, useState} from 'react'
import {useDispatch} from "react-redux";
import {useLocation, useMount} from "react-use";
import * as RR from "connected-react-router";
import {Modal} from 'react-bootstrap'
import PolicyModal from "./PolicyModal";

export default () => {
    const [prompt] = useState(!JSON.parse(localStorage.getItem('cookie')))
    const [show, setShow] = useState(false)
    const [showPolicy, setShowPolicy] = useState(false)
    useMount(() => {
        if(prompt)
        setTimeout(() => setShow(true), 1500)
    })
    const onClose = () => {
        setShow(false)
        localStorage.setItem('cookie', JSON.stringify(true))
    }
    return  <><Modal show={show} onHide={onClose} centered dialogAs={CookieDialog}>
                    <div className="cookies-modal">
                <div className="cookies-modal__content">
                    <h4 className="title">Об использовании файлов cookie на этом сайте</h4>
                    <p>Мы используем файлы cookie, чтобы улучшить взаимодействие сайта с пользователем. Посетите раздел <a onClick={() => setShowPolicy(true)}
                    style={{color: '#24a62d',
                        textDecoration: 'underline'}}>Cookie
                        Policy</a> для получения дополнительной информации. Нажимая на кнопку accept («принять»), вы подтверждаете
                        свое согласие с нашей политикой использования файлов cookie.</p>
                    <div className="btn-box">
                        <button className="btn-ok" type="button" data-bs-dismiss="modal" onClick={onClose} aria-label="Close">Принять cookie
                        </button>
                    </div>
                </div>
            </div>
    </Modal>
        <PolicyModal onHidePolicy={() => setShowPolicy(false)} showPolicy={showPolicy}/>
    </>
}

const CookieDialog = (props) => {
    return (<div className="modal-dialog modal-dialog-centered">

                {props.children}
        </div>
    )
}