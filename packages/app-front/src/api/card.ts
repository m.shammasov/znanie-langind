export type Card = {
    id?: string
    name: string
    activity: number
    photo: string
    character: number
    result: string
}