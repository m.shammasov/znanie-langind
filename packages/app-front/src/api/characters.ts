
import * as R from 'ramda'
import React from "react";

export type CharacterVO = {
    id?: number
    title?: string
    previewUrl: string
    pictureUrl: string

}

const getCharacter = (id: number) => ({
    previewUrl: `/img/chat/${id+1}/select-avatar.png 1x, /img/chat/${id+1}/select-avatar@2x.png 2x`
})

const characters: CharacterVO[] = R.times(getCharacter, 15)

export default characters
