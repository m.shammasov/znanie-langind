import React from 'react'
import {Provider} from 'react-redux'
import {HistoryContext} from './contexts'
import Routes from './Routes'
import CookiesModal from "./components/modals/CookiesModal";
import ModalShareCopy from "./components/modals/ModalShareCopy";
import ModalShareOk from "./components/modals/ModalShareOk";



export default ({store, history}) =>
    <Provider store={store}>
        <AppRender history={history} />
    </Provider>


const AppRender = ({history}) => {

    return  (
        <>
                <HistoryContext.Provider value={history}>
                    <Routes/>
                </HistoryContext.Provider>
        <CookiesModal></CookiesModal>
            <ModalShareCopy/>
            <ModalShareOk/>
        </>
    )
}
