import React, {useContext} from 'react'
import {connect, useSelector} from 'react-redux'
import {ConnectedRouter, Route, Switch} from '@sha/router'
import PageStart from "./components/start/PageStart";
import PageEnd from "./components/end/PageEnd";
import {Redirect} from "react-router";
import {HistoryContext} from "./contexts";
import {uiDuck} from "./store/ducks/uiDuck";
import PageShare from "./components/share/PageShare";


const Routes = () => {
    const history = useContext(HistoryContext)
    const uiState = useSelector(uiDuck.selectUI)

    if(uiState.busy.length)
        return null

    return (

            <ConnectedRouter history={history}>
                <Switch>
                    <Route
                        exact={false}
                        key={'start'}
                        path={'/start'}
                        component={PageStart}

                    />
                    <Route
                        exact={false}
                        key={'end'}
                        path={'/end'}
                        component={PageEnd}
                    />
                    <Route
                        exact={false}
                        key={'share'}
                        path={'/share'}
                        component={PageEnd}
                    />
                    <Route
                        key={'any'}

                    >
                        <Redirect to={'/start#first'} />
                    </Route>

                </Switch>
            </ConnectedRouter>


    )
}

export default Routes


