import React from "react";

export const createScrollStopListener = (element = window, callback: (e: React.UIEvent<HTMLElement>)=> any, timeout = 200) => {
    let removed = false;
    let handle = null;
    const onScroll = (e) => {
        if (handle) {
            clearTimeout(handle);
        }
        handle = setTimeout(callback, timeout || 200, e); // default 200 ms
    };
    element.addEventListener('scroll', onScroll);
    return () => {
        if (removed) {
            return;
        }
        removed = true;
        if (handle) {
            clearTimeout(handle);
        }
        element.removeEventListener('scroll', onScroll);
    };
};