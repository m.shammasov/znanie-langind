"use strict";

import gulp from "gulp";

const requireDir = require("require-dir"),
    paths = {
        views: {
            src: [
                "./src/views/*.pug",
                "./src/views/pages/*.pug"
            ],
            dist: "./dist/",
            prod: "./prod/",
            watch: [
                "./src/blocks/**/*.pug",
                "./src/views/**/*.pug"
            ]
        },
        styles: {
            src: [
                "./src/styles/bundle.{scss,sass}",
                "./src/styles/bundle2.{scss,sass}"
            ],
            dist: "./dist/styles/",
            prod: "./prod/styles/",
            watch: [
                "./src/blocks/**/*.{scss,sass}",
                "./src/styles/**/*.{scss,sass}"
            ]
        },
        scripts: {
            src: "./src/js/index.js",
            dist: "./dist/js/",
            prod: "./prod/js/",
            watch: [
                "./src/blocks/**/*.js",
                "./src/js/**/*.js"
            ]
        },
        images: {
            src: [
                "./src/img/**/*.{jpg,jpeg,png,gif,tiff,svg,webp}",
                "!./src/img/robot.svg",
                "!./src/img/favicon/*.{jpg,jpeg,png,gif,tiff,svg,webp}"
            ],
            dist: "./dist/img/",
            prod: "./prod/img/",
            watch: "./src/img/**/*.{jpg,jpeg,png,gif,svg,webp}"
        },
        webp: {
            src: [
                "./src/img/**/*.{tiff}",
                "!./src/img/favicon/*.{gif}"
            ],
            dist: "./dist/img/",
            prod: "./prod/img/",
            watch: [
                "./src/img/**/*.{tiff}",
                "!./src/img/favicon.{gif}"
            ]
        },
        sprites: {
            src: "./src/img/svg/*.svg",
            dist: "./dist/img/sprites/",
            prod: "./prod/img/sprites/",
            watch: "./src/img/svg/*.svg"
        },
        fonts: {
            src: "./src/fonts/**/*.{woff,woff2}",
            dist: "./dist/fonts/",
            prod: "./prod/fonts/",
            watch: "./src/fonts/**/*.{woff,woff2}"
        },
        favicons: {
            src: "./src/img/favicon/*.{svg,jpg,jpeg,png,gif,tiff}",
            dist: "./dist/img/favicon/",
            prod: "./prod/img/favicon/"
        },
        video: {
            src: "./src/video/**/*.{mp4,ogv,webm}",
            dist: "./dist/video/",
            prod: "./prod/video/",
            watch: [
                "./src/video/*.{mp4,ogv,webm}"
            ]
        },
        gzip: {
            src: "./src/.htaccess",
            dist: "./dist/",
            prod: "./prod/"
        },
        concat: {
            src: [
                "./src/concat/*.js"
            ],
            dist: "./dist/js/",
            prod: "./prod/js/",
            watch: [
                "./src/concat/*.js"
            ]
        },
        copy: {
            src: [
                "./src/for-copy/*.js",
                "./src/json/*.json",
           
            ],
            dist: "./dist/js/",
            prod: "./prod/js/",
            watch: [
                "./src/for-copy/*.js"
            ]
        },
    };
requireDir("./gulp-tasks/");

export { paths };

export const development = gulp.series("clean",
        //gulp.parallel(["views", "styles", "scripts", "images", "webp", "sprites", "fonts", "favicons"]),
        gulp.parallel(["views", "styles", "scripts", "images", "video", "sprites", "fonts", "favicons", "concat", "copy"]),
        gulp.parallel("serve")
    );

export const prod = gulp.series(
        //gulp.series(["views", "styles", "scripts", "images", "webp", "sprites", "fonts", "favicons", "gzip"]));
        //gulp.series(["views", "styles", "scripts", "images", "sprites", "fonts", "favicons", "gzip"]));
        //gulp.series(["views", "styles", "scripts", "concat", "copy", "sprites", "fonts", "favicons", "gzip"])
        gulp.series(["views", "styles", "scripts", "concat", "copy", "sprites", "images", "video", "fonts", "favicons", "gzip"])
    );

export default development;
