"use strict";

import { paths } from "../gulpfile.babel";
import gulp from "gulp";
import gulpif from "gulp-if";
import debug from "gulp-debug";
import yargs from "yargs";

const argv = yargs.argv,
    production = !!argv.production;

gulp.task("gzip", () => {
    return gulp.src(paths.gzip.src)
        .pipe(gulpif(production, gulp.dest(paths.gzip.prod)))
        .pipe(gulpif(!production, gulp.dest(paths.gzip.dist)))
        //.pipe(gulp.dest(paths.gzip.dist))
        .pipe(debug({
            "title": "GZIP config"
        }));
});