"use strict";

import { paths } from "../gulpfile.babel";
import gulp from "gulp";
import gulpif from "gulp-if";
import debug from "gulp-debug";
import yargs from "yargs";

const argv = yargs.argv,
    production = !!argv.production;

gulp.task("fonts", () => {
    return gulp.src(paths.fonts.src)
        .pipe(gulpif(production, gulp.dest(paths.fonts.prod)))
        .pipe(gulpif(!production, gulp.dest(paths.fonts.dist)))
        //.pipe(gulp.dest(paths.fonts.dist))
        .pipe(debug({
            "title": "Fonts"
        }));
});