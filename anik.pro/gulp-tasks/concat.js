"use strict";

import { paths } from "../gulpfile.babel";
import webpack from "webpack";
import webpackStream from "webpack-stream";
import gulp from "gulp";
import gulpif from "gulp-if";
import rename from "gulp-rename";
import browsersync from "browser-sync";
import concat from "gulp-concat";
import debug from "gulp-debug";
import yargs from "yargs";

const webpackConfig = require("../webpack.config.js"),
    argv = yargs.argv,
    production = !!argv.production;

webpackConfig.mode = production ? "production" : "development";
webpackConfig.devtool = production ? false : "source-map";

gulp.task("concat", () => {
    return gulp.src(paths.concat.src)
        .pipe(gulpif(production, rename({
            suffix: ".min"
        }), concat('all.min.js')))
        .pipe(gulpif(production, concat('all.min.js')))
        .pipe(gulpif(!production, concat('all.js')))
        //.pipe(concat('all.js'))
        .pipe(gulpif(production, gulp.dest(paths.concat.prod)))
        .pipe(gulpif(!production, gulp.dest(paths.concat.dist)))
        //.pipe(gulp.dest(paths.concat.dist))
        .pipe(debug({
            "title": "Concat"
        }))
        .on("end", browsersync.reload);
});