"use strict";

import { paths } from "../gulpfile.babel";
import gulp from "gulp";
import gulpif from "gulp-if";
import svg from "gulp-svg-sprite";
import debug from "gulp-debug";
import yargs from "yargs";
import browsersync from "browser-sync";

const argv = yargs.argv,
    production = !!argv.production;

gulp.task("sprites", () => {
    return gulp.src(paths.sprites.src)
        .pipe(svg({
            shape: {
                dest: "intermediate-svg"
            },
            mode: {
                stack: {
                    sprite: "../sprite.svg"
                }
            }
        }))
        .pipe(gulpif(production, gulp.dest(paths.sprites.prod)))
        .pipe(gulpif(!production, gulp.dest(paths.sprites.dist)))
        //.pipe(gulp.dest(paths.sprites.dist))
        .pipe(debug({
            "title": "Sprites"
        }))
        .on("end", browsersync.reload);
});