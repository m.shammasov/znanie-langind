"use strict";

import { paths } from "../gulpfile.babel";
import gulp from "gulp";
import gulpif from "gulp-if";
import rename from "gulp-rename";
import browsersync from "browser-sync";
import debug from "gulp-debug";
import yargs from "yargs";

const webpackConfig = require("../webpack.config.js"),
    argv = yargs.argv,
    production = !!argv.production;

webpackConfig.mode = production ? "production" : "development";
webpackConfig.devtool = production ? false : "source-map";

gulp.task("copy", () => {
    return gulp.src(paths.copy.src)
        .pipe(gulpif(production, rename({
            suffix: ".min"
        })))
        .pipe(gulpif(production, gulp.dest(paths.copy.prod)))
        .pipe(gulpif(!production, gulp.dest(paths.copy.dist)))
        //.pipe(gulp.dest(paths.copy.dist))
        .pipe(debug({
            "title": "Copy"
        }))
        .on("end", browsersync.reload);
});