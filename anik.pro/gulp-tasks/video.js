"use strict";

import { paths } from "../gulpfile.babel";
import gulp from "gulp";
import gulpif from "gulp-if";
import rename from "gulp-rename";
import debug from "gulp-debug";
import yargs from "yargs";

const webpackConfig = require("../webpack.config.js"),
    argv = yargs.argv,
    production = !!argv.production;

webpackConfig.mode = production ? "production" : "development";
webpackConfig.devtool = production ? false : "source-map";

gulp.task("video", () => {
    return gulp.src(paths.video.src)
        .pipe(gulpif(production, rename({
            suffix: ".min"
        })))
        .pipe(gulpif(production, gulp.dest(paths.video.prod)))
        .pipe(gulpif(!production, gulp.dest(paths.video.dist)))
        //.pipe(gulp.dest(paths.video.dist))
        .pipe(debug({
            "title": "Video"
        }));
});