function ready(callbackFunc) {
	if (document.readyState !== 'loading') {
		// Document is already ready, call the callback directly
		callbackFunc();
	}
	else if (document.addEventListener) {
		// All modern browsers to register DOMContentLoaded
		document.addEventListener('DOMContentLoaded', callbackFunc);
	}
	else {
		// Old IE browsers
		document.attachEvent('onreadystatechange', function () {
			if (document.readyState === 'complete') {
				callbackFunc();
			}
		});
	}
}
ready(function () {
	function logElementEvent(eventName, element) {
		console.log(
			Date.now(),
			eventName,
			element.getAttribute("data-src")
		);
	}
	var ll = new LazyLoad({
		elements_selector: ".lazy"
	});
	console.log('👍');
});