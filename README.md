# znanie-langind


## pre requirments

```
NodeJS v14.10 +
yarn 1.22 +
```

## Quick start prod
 
```
yarn install
yarn 
yarn front:stage # yarn front:build - for production build
yarn service
```
## Dev mode for frontend

```
yarn install
yarn 
yarn front:start
yarn service
```

### Packages structure
this is a node.js/react yarn monorepo with top-level defined dependencies at ```./package.json```.
Folder structure:
```
    .
    ├── anik.pro                # Style sources and Compiled pure html & css files.
    ├── packages                # monorepo workspaces
    │   ├── app-front           # Frontend react.js SPA - typescript used for source files
    │   ├── app-service         # Pure node.js app to server built by app-front static files 
    │   └── sha/*               # Utils packages 
    └── README.md               # Current readMe file 

```


